/*******************************************************************************
File: mqtt_dotTimeSync.c
Name: Hrishikesh Adigal <hadigal@sdsu.edu>
Desc: This program publishes a LoRa message to the Network Server topic which
      has details about the current timestamp in seconds from epoch of the App.
      server. This value is then forwarded to the end dev as a LoRa downlink msg
      which is used to sync its RTC to the application server's time.
Date: 05/25/2019
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include "MQTTClient.h" // MQTT-paho library header

// define ADDRESS, Client ID and TOPIC for the program
#define ADDR "tcp://localhost:1883"
#define CLID "MDOTGATEWAYPUB"
#define TOPIC1 "ENDDEVTIMESYNC"

// define the QOS and static PAYLOAD
#define QOS 1
#define PAYLOAD1 "2"

// define the username and pwd for the MQTT broker
const char *username = "mtcdt";
const char *pwd = "Q4opCvmQ";

// file name to log the published message tmst
static const char *fileName = "log_endDevTimeSync.txt" ;// static to hide it from other files
static char fullPath[128] = "/home/adigal/mqtt_test/mqtt_pub/";
volatile MQTTClient_deliveryToken dt;
FILE *fObj;

static bool sigFlg = true; // set ot fasle when interrup is caught
static time_t secs;
static uint8_t plFlg = 0;

// signal handler
static void intHandle(int dummy)
{
  sigFlg = false;
}

// helper function for message delivery for setCallback()
void delv(void *cliObj, MQTTClient_deliveryToken fdt)
{
  printf("Message with token:%d delivered\n",fdt);
  dt = fdt;
}

// helper function for msg arrvd for setCallback()
int msgarvd(void *cliObj, char *topic, int topicLen, MQTTClient_message *msg)
{
  printf("Doing Nothing\n");
  return 1;
}

// helper function in case of Connection is lost for setCallback()
void cLost(void *cliObj, char *reason)
{
  time_t temp = time(NULL);
  printf("\n::::::::::::::::Connection Lost::::::::::::::::\nReason:%s\n",reason);
  printf("temp:%s\n",asctime(localtime(&temp)));
  printf("\n:::::::::::::::::::::::::::::::::::::::::::::::\n");
}

int main(void)
{
  // MQTTClient_connectOptions_initializer is macro defined in MQTTClient.h
  MQTTClient obj; // just a void ptr used on create() call
  MQTTClient_connectOptions cntOpt = MQTTClient_connectOptions_initializer;
  MQTTClient_message pubMsg1 = MQTTClient_message_initializer; // struct initialized by macro

  //MQTTClient_message pubMsg2 = MQTTClient_message_initializer; // struct initialized by macro
  MQTTClient_deliveryToken local_dt; // local token used for pub()
  int retVal;
  char *pl = NULL;

  // Create a MQTT object
  MQTTClient_create(&obj,ADDR,CLID,MQTTCLIENT_PERSISTENCE_NONE,NULL);

  // init the struct params
  cntOpt.keepAliveInterval = 20;
  cntOpt.cleansession = 1;
  cntOpt.username = username;
  cntOpt.password = pwd;

  //setCallback() before connects
  MQTTClient_setCallbacks(obj,NULL,cLost,msgarvd,delv);

  //log file fullPath
  strcat(fullPath,fileName);

  // check if file is already present; if yes then overwriting the existing file
  if((access(fullPath,F_OK)) != -1)
  {
    printf("File %s already present @:%s .... overwriting the existing log\n",fileName,fullPath);
  }
  else
  {
    printf("Created a log for publish message timestamps @:%s\n",fullPath);
  }
  fObj = fopen(fullPath,"w");

  // implementing the signal handler
  struct sigaction sigAct;
  sigAct.sa_handler = &intHandle;
  sigaction(SIGINT,&sigAct,NULL);

  // continuously publish msg until there is a user interrupt
  while(sigFlg)
  {
    // connect to the msg broker created earlier
    retVal = MQTTClient_connect(obj,&cntOpt);
    if(retVal != MQTTCLIENT_SUCCESS)
    {
      printf("Error in connect()\t Ret Val:%d\n",retVal);
      return EXIT_FAILURE;
    }

    // store current mst in seconds from epoch for the application server
    time_t tmst = time(NULL);

    // declare the MQTTClient_publishMessage members based on the plFlg: if 0 then
    // then time sync else regular message
    if(plFlg == 0)
    {
      pl = (char *)calloc(11,sizeof(*pl));
      snprintf(pl,11,"%ld",tmst);
      plFlg = 1;
      pubMsg1.payload = pl;
      pubMsg1.payloadlen = strlen(pl);
    }
    else
    {
      pubMsg1.payload = PAYLOAD1;
      pubMsg1.payloadlen = strlen(PAYLOAD1);
    }

    pubMsg1.qos = QOS;
    pubMsg1.retained = 0;

    // declare the MQTTClient_publishMessage members;
    dt = 0;
    // publish message
    MQTTClient_publishMessage(obj,TOPIC1,&pubMsg1,&local_dt);
    printf("Published MSG:%s to mqtt ClientID:%s to Topic1:%s... waitng for delv. token\n",PAYLOAD1,CLID,TOPIC1);

    printf("local_dt:%d\n",local_dt);
    secs = time(NULL);
    printf("Time Stamp for successful delivery for TOPIC1[%s]:%s",TOPIC1,asctime(localtime(&secs)));

    if(dt == local_dt)
    {
      secs = time(NULL);
      printf("Time Stamp for successful delivery:%s",asctime(localtime(&secs)));
    }
    // log the published msg tmst
    fprintf(fObj,"%ld\n",secs);
    sleep(30);
  }

  // close everything
  printf("Disconnect client obj\n");
  MQTTClient_disconnect(obj,10000);//release()
  printf("Release the memory allocated for client\n");
  MQTTClient_destroy(&obj);//free()
  printf("Close the client obj\n");
  printf("Closing the log file\n");
  fclose(fObj);
  free(pl);

  return retVal;
}
