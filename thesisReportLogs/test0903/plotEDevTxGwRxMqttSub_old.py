import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import re
import time, datetime, pytz
from pytz import timezone
import dateutil.parser as dp
from statistics import mean,stdev
import queue

from math import ceil,floor

"""
File check
"""
# def checkFile(path):
#     if os.path.exists(path):
#         print("Found file:%s"%path)
#         return True
#     else:
#         print("File not found\n")
#         return False

"""
Plot the mmc Queue simulation for the inter arrival times for our test bed
"""
def interArrivalTimeDiff(file, devId,SF):
	diffTimes = []
	gwRxTmst = []
	totalDev = len(devId)

	arrTime =  parserFunct(file,devId)
	servTime =  parseServTmst(file,devId)
	arrRate = []
	servRate = []
	interArrTimes = []
	aTime = []
	sTime = []
	rho = []
	ls = []
	waitTime = []
	totalEntities = []
	timeHours = 6

	wTime = []
	tAt = []
	tSt = []
	waitList = []
	yax = []

	print("\n")
	for devItr in range(totalDev):
		total = len(arrTime[devItr])
		tm1 = []
		tm2 = []
		for itr in range(total):
			arrTime[devItr][itr] = int(arrTime[devItr][itr])
			servTime[devItr][itr] = int(servTime[devItr][itr])
			tm1.append(arrTime[devItr][itr] - arrTime[devItr][0])
			tm2.append(servTime[devItr][itr] - servTime[devItr][0])
		aTime.append(tm1)
		sTime.append(tm2)

		diffArr = arrTime[devItr][-1] - arrTime[devItr][0]
		diffToa = servTime[devItr][-1] - servTime[devItr][0]

		diffArr /= (3600)
		diffToa /= (3600)

		arrRate.append(total/diffArr)
		servRate.append(total/diffToa)

		rho.append(float(floor(arrRate[devItr])/ceil(servRate[devItr])))
		tmpLs = rho[devItr]/(1-rho[devItr])
		ls.append(tmpLs)
		tmpWs = tmpLs/arrRate[devItr]
		waitTime.append(tmpWs)
		print("Dev:{} => rho (arrRate/servRate):{} Ls:{} Ws(waitTime):{}\n".format(devId[devItr],rho[devItr],tmpLs,tmpWs))
		totalEntities.append(int(np.random.poisson(arrRate[devItr])*timeHours))


	"""
	Compute inter arrival times and arrival time
	"""
	# print("aTime:{}\n".format(aTime))
	for devItr in range(totalDev):
		iat = []
		at = []
		st = []
		wt = []
		for entities in range(totalEntities[devItr]):
			val1 = np.random.exponential(1/arrRate[devItr])*(60*60)
			val2 = np.random.exponential(1/servRate[devItr])*(60*60)
			st.append(val2)

			if entities != 0:
				iat.append(int(val1))
				at.append(at[entities-1]+iat[entities])
				wt.append(at[entities] + waitTime[devItr])
			else:
				iat.append(0)
				at.append(0)
				wt.append(0)

		interArrTimes.append(iat)
		tAt.append(at)
		tSt.append(st)
		waitList.append(wt)

	"""
	queue sim
	"""
	fig = plt.figure(2)
	for devItr in range(totalDev):
		cp = None
		en = aTime[devItr][0]
		itr = 0
		ytmp = []
		serviced = 0
		serv = False
		mm1Queue = queue.Queue()

		while ((itr < len(aTime[devItr])) and (en < (timeHours*60*60))):
			en = aTime[devItr][itr]
			if serv and (cp < len(tSt[devItr])):
				tSt[devItr][cp] -= 1
				if tSt[devItr][cp] <= 0:
					serv = False
					serviced += 1

			mm1Queue.put(en)

			if not serv and not mm1Queue.empty():
				cp = mm1Queue.get()
				serv = True

			sw = 0

			if serviced == 0:
				ytmp.append(0)
			else:
				for ens in range(serviced):
					sw += waitList[devItr][ens]
				ytmp.append(sw/(serviced*60*60))

			itr += 1
			# en = aTime[devItr][itr]

		yax.append(ytmp)

		lim = None
		if totalEntities[devItr] > len(yax[devItr]):
			lim = len(yax[devItr])
		else:
			lim = totalEntities[devItr]

		lb = "Dev:{} SF:{}".format(devItr+1,SF[devItr])

		xax = [(tmp+1) for tmp in range(lim)]
		plt.plot(xax,yax[devItr][:lim],label=lb)
		# print(yax[devItr][:lim])

	plt.ylabel("Avg. Wait Time in Seconds")
	plt.xlabel("Total LoRa packets In The System")
	plt.rcParams.update({'font.size': 14})
	plt.title('# of LoRa packets in the system vs Avg. wait time in secs. simulated using M/M/1 queue')

	ax = plt.subplot(111)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# plt.gca().yaxis.set_major_locator (plt.NullLocator())
	plt.grid(True)
	plt.show()

	ret = [arrRate,servRate,rho,ls,waitTime]
	return ret

"""
Get the mean and standard deviation
"""
def getMeanStd(endDevTmst,serverTmst,devName=['dev1','dev2']):

	gwRx = []

	for devItr in range(len(endDevTmst)):
		deltaT = []
		for itr in range(len(endDevTmst[devItr])):
			delta = serverTmst[devItr][itr] - endDevTmst[devItr][itr]
			deltaT.append(delta)
		gwRx.append(deltaT)

	devMean = []
	std = []
	print("\n")
	for itr in range(len(devName)):
		# print("gwRx[{}]:{}".format(itr,gwRx[itr]))
		devMean.append(mean(gwRx[itr]))
		std.append(stdev(gwRx[itr]))
		print("Dev:{} -> Mean: {}\t Std. Dev.:{}\n".format(devName[itr],devMean[itr],std[itr]))

	return devMean,std

"""
Plot the gw rx timestamps and toa timestamps
"""
def plot_predicted(devId,path,tmstEndDev,yax,SF):

	"""
	Gateway RX tmst
	"""
	devTmst = parserFunct(path,devId)

	dTmst = devTmst

	"""
	gw rx progessive tmst for plot
	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	"""
	gTime = []
	for devItr in range(len(dTmst)):
		tmp = []
		for itr in range(len(dTmst[devItr])):
			tmp.append(dTmst[devItr][itr] - tmstEndDev[devItr][0])
		gTime.append(tmp)

	# print("gtime:{}\n".format(gTime))

	"""
	y axis for plot
	"""
	y_axis = []
	for devItr in range(len(gTime)):
		tmp = np.array([yax[devItr] for i in range(len(gTime[devItr][:20]))])
		y_axis.append(tmp)

	"""
	Compute mean and std for each device
	"""
	devMean,std = getMeanStd(tmstEndDev,dTmst,devId)

	"""
	Plot
	"""
	colorList = ['g','g','g','g']
	markerList = ['+','x','D','2']

	for itr in range(len(devId)):
		# lb = "GW Rx(UpLink) Dev:{}".format(devId[itr][18:23]) + ' ' + SF[itr]+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[itr])+'\n'+'Std. Dev:'+"{:.3f}".format(std[itr])
		lb = "GW Rx(UpLink) Dev:{}".format(itr+1) + '@' + SF[itr]+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[itr])+'\n'+'Std. Dev:'+"{:.3f}".format(std[itr])
		plt.rcParams.update({'font.size': 11})
		plt.scatter(np.array(gTime[itr][:20]),y_axis[itr], label=lb, color=colorList[itr],marker=markerList[itr])

	plt.rcParams.update({'font.size': 14})
	plt.xlabel('Time in secs')
	plt.ylabel('DR0 vs DR1 vs DR2 vs DR3')

	return [dTmst,gTime]

def plotEndDevTmst(file,yax,toaYax,SF,thToA,devId):
	"""
	# end dev tx
	"""
	parseTime = parserEndDev(file,devId)
	tmst = parseTime

	"""
	progessive Tx tmst end dev
	"""
	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	eDevTmst = []
	toaTmst = []
	for devItr in range(len(tmst)):
		tmp = []
		toaTmp = []
		val = tmst[devItr][0]
		thToaVal = thToA[SF[devItr]]/1000
		for itr in range(len(tmst[devItr])):
			tmp.append(tmst[devItr][itr] - val)
			toaTmp.append(((tmst[devItr][itr] - val) + thToaVal))
		eDevTmst.append(tmp)
		toaTmst.append(toaTmp)

	"""
	Y axis for ToA and normal plots
	"""
	y_axis = []
	y_axis_toa = []
	colorListTmst = ['b','b','b','b']
	colorListToa = ['k','k','k','k']
	markerListTmst = ["*","h","8","^"]
	markerListToa = ['p','p','p','p']

	"""
	Plot the data
	"""
	for devItr in range(len(eDevTmst)):
		eDevTmst[devItr] =  np.array(eDevTmst[devItr][:20])
		toaTmst[devItr] = np.array(toaTmst[devItr][:20])
		tmp = np.array([yax[devItr] for i in range(len(eDevTmst[devItr]))])
		y_axis.append(tmp)
		tmp2 = np.array([toaYax[devItr] for i in range(len(toaTmst[devItr]))])
		y_axis_toa.append(tmp2)

		lb1 = "EndDev Tx(UpLink) Dev:{}".format(devItr+1) + '@' + SF[devItr]
		toalb1 = "Gw Rx based on th. ToA Dev:{}".format(devItr+1) + '@' + SF[devItr]

		plt.rcParams.update({'font.size': 11})
		plt.scatter(eDevTmst[devItr],y_axis[devItr], label=lb1, color=colorListTmst[devItr],marker=markerListTmst[devItr])
		plt.scatter(toaTmst[devItr],y_axis_toa[devItr], label=toalb1, color=colorListToa[devItr],marker=markerListToa[devItr])

	plt.rcParams.update({'font.size': 14})
	return [tmst,eDevTmst,toaTmst]

def parseServTmst(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)mqtt Tx:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						res = obj.group(2);
						t_sec = res.split()[0]
						dev.append(int(t_sec))

				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False

def parserEndDev(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)Tx Time:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						res = obj.group(2)
						t_sec = res.split()[0]
						dev.append(int(t_sec))

				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False


def fileExists(file):
    if os.path.exists(file):
        print("file:%s found\n"%file)
        return 1
    else:
        print("File:%s not found\n"%file)
        return -1


def parserFunct(file,devId):
	devREx = r'(.*)Device ID:(.*)'
	mtimeRegEx = r'(.*)Msg Time: (.*)'

	dev2 = []
	dev1 = []
	devParse = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(mtimeRegEx,line,re.M|re.I)
						res = obj.group(2)
						tp = dp.parse(res.split()[0])
						tmp = tp.astimezone(timezone('US/Pacific'))
						t_sec = tmp.strftime('%s')
						dev.append(int(t_sec))
				devParse.append(dev)
				file.seek(0)
		return devParse
	else:
		return False

'''
# this is for the toa item in the marconi log file
'''
def parserFunctToa(file,devList):

	regex_dev_toa = r'(.*)TOA:(.*)'
	dev1_val = []
	dev2_val = []
	itr = 0;
	if fileExists(file) != -1:
		with open(file,'r') as file:
			for line in file:
				if devList[0] in line and "TOA" in line:
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev1_val.append(int(res.split()[0]))
				elif devList[1] in line and "TOA" in line:
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev2_val.append(int(res.split()[0]))
		return (dev1_val,dev2_val)
	else:
		return False

'''
To plot the mqtt_sub timestamps
'''
def creatTimeList(file,devId,eDevTmst=None):
	regex_dev_sub_time = r'(.*)Msg Time:(.*)'
	parseTmst = []

	with open(file,'r') as fp:
		for devItr in range(len(devId)):
			dev = []
			if eDevTmst is not None:
				val = eDevTmst[devItr][0]
			for line in fp:
				if devId[devItr] in line:
					obj = re.search(regex_dev_sub_time,line,re.M|re.I)
					res = obj.group(2)
					if eDevTmst is not None:
						dev.append((int(res.split()[0])) - val)
					else:
						dev.append((int(res.split()[0])))
			parseTmst.append(dev)
			fp.seek(0)
	return parseTmst

def plot(filePath,devId,eDevTmst,yax,SF):
	retFlag = 0
	retFlag += fileExists(filePath)

	if retFlag < 1:
		return -1


	tmst = creatTimeList(filePath,devId,eDevTmst)

	# print("Delta Time between End Dev1[fe37] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev1[:20]))
	# print("Delta Time between End Dev2[fe36] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev2[:20]))
	y_axis = []
	colorList = ['r','r','r','r']
	plotCList = ['m','m','m','m']
	mList = ["o","v","s","3"]

	tmstnp = []
	for devItr in range(len(devId)):
		tmptmst = np.array(tmst[devItr])
		tmp = np.array([yax[devItr] for i in range(len(tmst[devItr]))])
		y_axis.append(tmp)
		tmstnp.append(tmptmst)

		# lb1 = 'mqtt sub Dev:..{}'.format(devId[devItr][18:23]) + ' ' + SF[devItr]
		lb1 = 'mqtt sub Dev:{}'.format(devItr+1) + '@' + SF[devItr]

		plt.plot(tmstnp[devItr][:20], y_axis[devItr][:20], color=plotCList[devItr], linestyle='None', markersize = 10.0)
		plt.scatter(tmstnp[devItr][:20],y_axis[devItr][:20],label = lb1 ,color=colorList[devItr],marker=mList[devItr])

	return tmstnp


def helper(file,devId,thToA,SF):
	"""
	plot various subplots for tx time vs delay in rx
	"""

	eDevTmst = parserEndDev(file[2],devId)
	gwTmst = parserFunct(file[0],devId)
	aServTmst = creatTimeList(file[1],devId)
	totalDev = range(len(devId))

	#pg tmst eDev
	thToaTmst = []
	for devItr in totalDev:
		tmp = []
		thToaVal = thToA[SF[devItr]]/1000
		for itr in range(len(eDevTmst[devItr])):
			tmp.append(eDevTmst[devItr][itr] + thToaVal)
		thToaTmst.append(tmp)

	thProg = []
	for devItr in totalDev:
		tmp = []
		for itr in range(len(thToaTmst[devItr])):
			tmp.append(thToaTmst[devItr][itr] - eDevTmst[devItr][itr])
		thProg.append(tmp)

	gwProg = []
	for devItr in totalDev:
		tmp = []
		for itr in range(len(gwTmst[devItr])):
			tmp.append(gwTmst[devItr][itr] - eDevTmst[devItr][itr])
		gwProg.append(tmp)

	asProg = []
	for devItr in totalDev:
		tmp = []
		for itr in range(len(aServTmst[devItr])):
			tmp.append(aServTmst[devItr][itr] - eDevTmst[devItr][itr])
		asProg.append(tmp)

	lb = ["Th. ToA Tmst in sec","GW Rx Tmst in sec","MQTT sub Tmst in sec"]

	mk = [["o","v","s","3"],['+','*',"x","2"],["8","^",'1','D']]
	c = ['b','m','g','r','c']
	yax = [thProg,gwProg,asProg]

	fig = plt.figure(3)
	ax = plt.subplot(444)

	for pItr in range(3):
		y = yax[pItr]
		cl = c[pItr]
		m = mk[pItr]
		for itr in totalDev:
			plt.subplot(4,1,itr+1)

			if pItr == 2:
				plt.plot(np.array(eDevTmst[itr][:20]),np.array(y[itr][:20]),'r--',label=lb[pItr],marker=m[itr], color=cl)
			else:
				plt.plot(np.array(eDevTmst[itr][:20]),np.array(y[itr][:20]),'r--',marker=m[itr], color=cl)

			if pItr == 2:
				plt.xlabel("Tx timestamps in seconds from Epoch")
				plt.ylabel("Delay in seconds")
				plt.rcParams.update({'font.size': 12})
				plt.title("Dev:{} @ {}".format(itr+1,SF[itr]))
				# Shrink current axis by 20%
				box = ax.get_position()
				ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

				# Put a legend to the right of the current axis
				# plt.gca().yaxis.set_major_locator(plt.NullLocator())
				plt.gca().legend(("Th. ToA Tmst Delay in sec","GW Rx Tmst Delay in sec","MQTT sub Tmst Delay in sec"),loc='center left', bbox_to_anchor=(1, 0.5))
				# ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
				plt.grid(True)

	plt.rcParams.update({'font.size': 12})
	fig.suptitle('Tx Time vs Rx Delay plot for all the end devices')
	plt.rcParams.update({'font.size': 16})
	text_str = 'The plot represents a four subplots for different devices showing the delay between the end dev tx tmst and rx delay measured in seconds at GW and application sever\n'
	fig.text(.5,0.01,text_str,wrap=True,ha='center')




	# plt.gca().yaxis.set_major_locator(plt.NullLocator())
	plt.show()

	# plt.gca()
	# plt.show()


if __name__ == '__main__':

	filePath1 = ['/home/iot/Desktop/thesisRepoBitBucket/test0903/gwTmstLog0903.txt', '/home/iot/Desktop/thesisRepoBitBucket/test0903/logMqttSub0903.txt','/home/iot/Desktop/thesisRepoBitBucket/test0903/endDevTmstLog0903.txt']

	devId = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36','00-80-00-00-04-00-98-02','00-80-00-00-04-00-98-2b']
	axis_sub = [1,3]
	axis_gw = [2,4]
	SF = ['SF7','SF10','SF8','SF9']

	thToA = {'SF10':288.847992,'SF9':328.223999,'SF8':369.912018,'SF7':379.016022,'SF8BW500':163.968002}

	fig = plt.figure(1)
	ax = plt.subplot(111)


	endDevTmst = plotEndDevTmst(filePath1[2],[4,8,12,16],[3,7,11,15],SF,thToA,devId)
	mqttSubTmst = plot(filePath1[1],devId,endDevTmst[0],[1,5,9,13],SF) # mqtt sub/pub plot
	gwTmst = plot_predicted(devId,filePath1[0],endDevTmst[0],[2,6,10,14],SF) # gw Rx

	plt.rcParams.update({'font.size': 12})
	plt.title('End Dev Tx vs Gateway Rx Time vs mqtt sub(marconi) time for 4 End Dev Transmitting max PL operating at Data Rates [DR0,DR1,DR2 & DR3]')
	plt.rcParams.update({'font.size': 16})
	text_str = 'Data Transfer Path: mdot Tx -> Gateway Rx -> Gateway MQTT pub -> marconi MQTT sub\nThe above graph is the representation of 1st 20 tmst. from start of Tx from End Dev\n'
	fig.text(.5,0.01,text_str,wrap=True,ha='center')

	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


	plt.gca().yaxis.set_major_locator (plt.NullLocator())
	plt.grid(True)
	plt.show()

	# deltaTime,std = getMeanStd(endDevTmst[0],gwTmst[0],devId)
	timeDiff = interArrivalTimeDiff(filePath1[0], devId,SF)
	# print("time Diff:\n{}\n".format(timeDiff))

	# plot subplots for tx time vs delay in rx for different end dev
	helper(filePath1,devId,thToA,SF)
