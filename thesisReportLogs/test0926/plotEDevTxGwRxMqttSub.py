import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import re
import time, datetime, pytz
from pytz import timezone
import dateutil.parser as dp
from statistics import mean,stdev
import queue

from math import ceil,floor

"""
File check
"""
# def checkFile(path):
#     if os.path.exists(path):
#         print("Found file:%s"%path)
#         return True
#     else:
#         print("File not found\n")
#         return False

"""
Plot the mmc Queue simulation for the inter arrival times for our test bed
"""
def interArrivalTimeDiff(file, devId,SF):
	diffTimes = []
	gwRxTmst = []
	totalDev = len(devId)

	arrTime =  parserFunct(file,devId)
	servTime =  parseServTmst(file,devId)
	arrRate = []
	servRate = []
	interArrTimes = []
	aTime = []
	sTime = []
	rho = []
	ls = []
	waitTime = []
	totalEntities = []
	timeHours = 6

	wTime = []
	tAt = []
	tSt = []
	waitList = []
	yax = []

	print("\n")
	for devItr in range(totalDev):
		total = len(arrTime[devItr])
		print("devId:{} total:{}\n".format(devId[devItr],total))
		tm1 = []
		tm2 = []
		for itr in range(total):
			arrTime[devItr][itr] = int(arrTime[devItr][itr])
			servTime[devItr][itr] = int(servTime[devItr][itr])
			tm1.append(arrTime[devItr][itr] - arrTime[devItr][0])
			tm2.append(servTime[devItr][itr] - servTime[devItr][0])
		aTime.append(tm1)
		sTime.append(tm2)

		diffArr = arrTime[devItr][-1] - arrTime[devItr][0]
		diffToa = servTime[devItr][-1] - servTime[devItr][0]
		print("diffArr:{}\ndiffToa:{}\n".format(diffArr,diffToa))

		diffArr /= (3600)
		diffToa /= (3600)
		print("....diffArr:{}\ndiffToa:{}\n".format(diffArr,diffToa))

		arrRate.append(total/diffArr)
		servRate.append(total/diffToa)

		rho.append(float(floor(arrRate[devItr])/ceil(servRate[devItr])))
		arrRate[devItr] = floor(arrRate[devItr])
		servRate[devItr] = ceil(servRate[devItr])

		tmpLs = float(rho[devItr]/(1-rho[devItr]))
		ls.append(tmpLs)
		tmpWs = tmpLs/arrRate[devItr]
		waitTime.append(tmpWs)
		print("Dev:{} => rho (arrRate/servRate):{} Ls:{} Ws(waitTime):{}\n".format(devId[devItr],rho[devItr],tmpLs,tmpWs))
		totalEntities.append(int(np.random.poisson(arrRate[devItr])*timeHours))

	print("(servRate):{}\n(ls):{}\n(arrRate):{}\n".format(np.array(servRate),np.array(ls),np.array(arrRate)))

	# fig_upbound = plt.figure(2)
	# fig = plt.figure(3)
	sfig, ax = plt.subplots()
	# plt.subplot(1,2,1)
	# lm_mu = [np.array(arrRate),np.array(servRate)]
	# ln = ['--',':']
	# lb = ["lambda","mu"]
	# mk = ['*','p']
	# cr = ['k','b']
	#
	# for tpItr in range(2):
	# 	plt.plot(lm_mu[tpItr],np.array(ls),ln[tpItr],label=lb[tpItr],marker=mk[tpItr], color=cr[tpItr])
	#
	# 	# plt.plot(lm_mu[tpItr],np.array(ls),'--',label="lambda",marker='*', color='k')
	# 	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# 	# plt.title("Arrival Rate vs Expected Number of Packets in the System")
	# 	# plt.ylabel("Ls - Expected Number of Packets in the System",fontweight='bold',fontsize='14')
	# 	# plt.xlabel("Lambda - Arrival Rate [# of packets arrived/hr.]",fontweight='bold',fontsize='14')
	# 	# font = {'family' : 'monospace','weight' : 'bold','size': 14}
	# 	# plt.rc('font',**font)
	# 	# # Shrink current axis by 20%
	# 	# box = ax.get_position()
	# 	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	# 	# # Put a legend to the right of the current axis
	# 	# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# 	# plt.gca().legend(loc='best', bbox_to_anchor=(1, 0.5))
	# 	# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# 	# plt.grid(True)
	#
	# 	# plt.subplot(1,2,2)
	# 	# plt.plot(np.array(servRate),np.array(ls),':',label="mu",marker='p', color='b')
	# 	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# 	# plt.title("Service Rate vs Expected Number of Packets in the System")
	# 	# plt.ylabel("Ls - Expected Number of Packets in the System",fontweight='bold',fontsize='14')
	# 	# plt.xlabel("Mu - Service Rate [# of packets serviced/hr.]",fontweight='bold',fontsize='14')
	# 	# font = {'family' : 'monospace','weight' : 'bold','size': 14}
	# 	# plt.rc('font',**font)
	# 	# # Shrink current axis by 20%
	# 	# box = ax.get_position()
	# 	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	# 	# # Put a legend to the right of the current axis
	# 	# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# 	# plt.gca().legend(loc='best', bbox_to_anchor=(1, 0.5))
	# 	# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# 	# plt.grid(True)
	#
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# plt.title("Arrival Rate vs Expected Number of Packets in the System")
	# plt.ylabel("Ls - Expected Number of Packets in the System",fontweight='bold',fontsize='14')
	# plt.xlabel("Lambda - Arrival Rate [# of packets arrived/hr.] and\nMu - Service Rate [# of packets serviced/hr]",fontweight='bold',fontsize='14')
	# font = {'family' : 'monospace','weight' : 'bold','size': 14}
	# plt.rc('font',**font)
	# # Shrink current axis by 20%
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	# # Put a legend to the right of the current axis
	# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# plt.gca().legend(loc='best', bbox_to_anchor=(1, 0.5))
	# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# plt.grid(True)
	#
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# # fig_upbound.suptitle('Lambda - Arrival Rate and Mu - Service Rate represented as\nfunction of Ls - Expected # of packets in the system')
	# fig.suptitle('Lambda - Arrival Rate and Mu - Service Rate represented as\nfunction of Ls - Expected # of packets in the system')
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# text_str = 'The plots represents 1) The upper bond for end device UpLink Rate transmitting at [SF7,SF8,SF9,SF10] by showing Lambda and Mu as function of Ls in plot 1\n2)Total estimated packets serviced by the system using M/M/1:inf.:inf. Queuing Model in plot2'
	# # fig_upbound.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
	# fig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')

	"""
	Compute inter arrival times and arrival time
	"""
	for devItr in range(totalDev):
		iat = []
		at = []
		st = []
		wt = []
		for entities in range(totalEntities[devItr]):
			val1 = np.random.exponential(1/arrRate[devItr])*(60*60)
			val2 = np.random.exponential(1/servRate[devItr])*(60*60)
			st.append(val2)

			if entities != 0:
				iat.append(int(val1))
				at.append(at[entities-1]+iat[entities])
				wt.append(at[entities] + waitTime[devItr])
			else:
				iat.append(0)
				at.append(0)
				wt.append(0)

		interArrTimes.append(iat)
		tAt.append(at)
		tSt.append(st)
		waitList.append(wt)

	"""
	queue sim
	"""
	# fig = plt.figure(3)
	# plt.subplot(1,2,2)
	# ax2 = None
	for devItr in range(totalDev):
		cp = None
		en = aTime[devItr][0]
		itr = 0
		ytmp = []
		serviced = 0
		serv = False
		mm1Queue = queue.Queue()

		while ((itr < len(aTime[devItr])) and (en < (timeHours*60*60))):
			if serv and (cp < len(tSt[devItr])):
				tSt[devItr][cp] -= 1
				if tSt[devItr][cp] <= 0:
					serv = False
					serviced += 1

			mm1Queue.put(en)

			if not serv and not mm1Queue.empty():
				cp = mm1Queue.get()
				serv = True

			sw = 0

			if serviced == 0:
				ytmp.append(0)
			else:
				for ens in range(serviced):
					sw += waitList[devItr][ens]
				ytmp.append(sw/(serviced*60*60))

			itr += 1
			if ((itr < len(aTime[devItr]))):
				en = aTime[devItr][itr]

		yax.append(ytmp)

		lim = None
		if totalEntities[devItr] > len(yax[devItr]):
			lim = len(yax[devItr])
		else:
			lim = totalEntities[devItr]

		lb = "Dev:{} SF:{}".format(devItr+1,SF[devItr])

		xax = [(tmp+1) for tmp in range(lim)]
		ax.plot(xax,yax[devItr][:lim],label=lb)

		# print(yax[devItr][:lim])
	ax2 = ax.twinx()
	# ax2.plot(xax,rho[devItr])
	ax2.plot(np.array(ls),np.array(rho),'ro',label='rho')
	devItr = 0
	for xy in zip(ls,rho):
		plt.rcParams.update({'font.size': 10,'font.weight': 'bold','font.family':'monospace'})
		ax2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')
		devItr += 1

	print("rho:{}".format(rho))
	# for devItr in range(totalDev):
	# 	for xy in zip(ls,rho):
	# 		ax2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')

	# sfig.tight_layout()
	# plt.ylabel("Average Metering Payload Delay[seconds]",fontsize=14,fontweight='bold')
	ax.set_ylabel("Average Metering Payload Delay[seconds]",fontsize=16,fontweight='bold')
	ax.set_xlabel("Total LoRa packets In The System\n[LoRa Gateway + Application Server]",fontsize=14,fontweight='bold')
	ax2.set_ylabel("rho (arrRate[lambda]/servRate[mu]) - Utilization Factor",fontsize=16,fontweight='bold')

	font = {'family' : 'monospace','weight' : 'bold','size': 16}
	plt.rc('font',**font)
	plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
	plt.title('# of LoRa packets in the system vs Avg. Metering Payload Delay[seconds] simulated using M/M/1 queue\nUtilization Factor - rho vs Avg. Metering Payload Delay[seconds]')

	# ax = plt.subplot(111)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	box1 = ax2.get_position()
	ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='best', bbox_to_anchor=(1, 0.5))
	ax2.legend(loc='best', bbox_to_anchor=(1, 0.25))
	# plt.gca().yaxis.set_major_locator (plt.NullLocator())
	# text_str = 'The plots represents 1)Total estimated packets serviced by the system using M/M/1:inf.:inf. Queuing Model with left y-axis\n2)) The upper bond for end device UpLink Rate transmitting at [SF8,SF7,SF9,SF10] by showing rho < 1 as function of Ls in plot with right y-axis\n'
	# # fig_upbound.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
	# sfig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
	plt.grid(True)
	plt.show()

	print("Ws:{}\nLs:{}\n".format(waitTime,ls))
	ret = [arrRate,servRate,rho,ls,waitTime]
	return ret

"""
Get the mean and standard deviation
"""
def getMeanStd(endDevTmst,serverTmst,devName=['dev1','dev2']):

	gwRx = []

	for devItr in range(len(endDevTmst)):
		deltaT = []
		for itr in range(len(endDevTmst[devItr])):
			delta = serverTmst[devItr][itr] - endDevTmst[devItr][itr]
			deltaT.append(delta)
		gwRx.append(deltaT)

	devMean = []
	std = []
	print("\n")
	for itr in range(len(devName)):
		devMean.append(mean(gwRx[itr]))
		std.append(stdev(gwRx[itr]))
		print("Dev:{} -> Mean: {}\t Std. Dev.:{}\n".format(devName[itr],devMean[itr],std[itr]))

	return devMean,std

"""
Plot the gw rx timestamps and toa timestamps
"""
def plot_predicted(devId,path,tmstEndDev,yax,SF):

	"""
	Gateway RX tmst
	"""
	devTmst = parserFunct(path,devId)

	dTmst = devTmst

	"""
	gw rx progessive tmst for plot
	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	"""
	gTime = []
	for devItr in range(len(dTmst)):
		tmp = []
		for itr in range(len(dTmst[devItr])):
			tmp.append(dTmst[devItr][itr] - tmstEndDev[devItr][0])
		gTime.append(tmp)

	# print("gtime:{}\n".format(gTime))

	"""
	y axis for plot
	"""
	y_axis = []
	for devItr in range(len(gTime)):
		# tmp = np.array([yax[devItr] for i in range(len(gTime[devItr][:20]))])
		tmp = [yax[devItr] for i in range(len(gTime[devItr][:20]))]
		y_axis.append(tmp)

	# print("y_axis:{}\ngTime[itr][:20]:{}\n".format(y_axis,gTime[:20]))
	"""
	Compute mean and std for each device
	"""
	devMean,std = getMeanStd(tmstEndDev,dTmst,devId)

	"""
	Plot
	"""
	colorList = ['g','g','g','g']
	markerList = ['+','x','D','2']

	for itr in range(len(devId)):
		# lb = "GW Rx(UpLink) Dev:{}".format(devId[itr][18:23]) + ' ' + SF[itr]+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[itr])+'\n'+'Std. Dev:'+"{:.3f}".format(std[itr])
		lb = "GW Rx(UpLink) Dev:{}".format(itr+1) + '@' + SF[itr]+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[itr])+'\n'+'Std. Dev:'+"{:.3f}".format(std[itr])
		plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
		plt.scatter(np.array(gTime[itr][:20]),np.array(y_axis[itr]), label=lb, color=colorList[itr],marker=markerList[itr])

	plt.rcParams.update({'font.size': 18,'font.weight': 'bold','font.family':'monospace'})
	plt.xlabel('Time in secs',fontsize=14,fontweight='bold')
	plt.ylabel('DR0 vs DR1 vs DR2 vs DR3',fontsize=14,fontweight='bold')

	return [dTmst,gTime]

def plotEndDevTmst(file,yax,toaYax,SF,thToA,devId):
	"""
	# end dev tx
	"""
	parseTime = parserEndDev(file,devId)
	tmst = parseTime

	"""
	progessive Tx tmst end dev
	"""
	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	eDevTmst = []
	toaTmst = []
	for devItr in range(len(tmst)):
		tmp = []
		toaTmp = []
		val = tmst[devItr][0]
		thToaVal = thToA[SF[devItr]]/1000
		for itr in range(len(tmst[devItr])):
			tmp.append(tmst[devItr][itr] - val)
			toaTmp.append(((tmst[devItr][itr] - val) + thToaVal))
		eDevTmst.append(tmp)
		toaTmst.append(toaTmp)

	"""
	Y axis for ToA and normal plots
	"""
	y_axis = []
	y_axis_toa = []
	colorListTmst = ['b','b','b','b']
	colorListToa = ['k','k','k','k']
	markerListTmst = ["*","h","8","^"]
	markerListToa = ['p','p','p','p']

	"""
	Plot the data
	"""
	for devItr in range(len(eDevTmst)):
		eDevTmst[devItr] =  np.array(eDevTmst[devItr][:20])
		toaTmst[devItr] = np.array(toaTmst[devItr][:20])
		tmp = np.array([yax[devItr] for i in range(len(eDevTmst[devItr]))])
		y_axis.append(tmp)
		tmp2 = np.array([toaYax[devItr] for i in range(len(toaTmst[devItr]))])
		y_axis_toa.append(tmp2)

		lb1 = "EndDev Tx(UpLink) Dev:{}".format(devItr+1) + '@' + SF[devItr]
		toalb1 = "Gw Rx based on th. ToA Dev:{}".format(devItr+1) + '@' + SF[devItr]

		plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
		plt.scatter(eDevTmst[devItr],y_axis[devItr], label=lb1, color=colorListTmst[devItr],marker=markerListTmst[devItr])
		plt.scatter(toaTmst[devItr],y_axis_toa[devItr], label=toalb1, color=colorListToa[devItr],marker=markerListToa[devItr])

	plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
	return [tmst,eDevTmst,toaTmst]

def parseServTmst(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)mqtt Tx:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						res = obj.group(2);
						t_sec = res.split()[0]
						dev.append(int(t_sec))

				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False

def parserEndDev(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)Tx Time:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						res = obj.group(2)
						t_sec = res.split()[0]
						dev.append(int(t_sec))

				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False


def fileExists(file):
    if os.path.exists(file):
        print("file:%s found\n"%file)
        return 1
    else:
        print("File:%s not found\n"%file)
        return -1


def parserFunct(file,devId):
	devREx = r'(.*)Device ID:(.*)'
	mtimeRegEx = r'(.*)Msg Time: (.*)'

	dev2 = []
	dev1 = []
	devParse = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(mtimeRegEx,line,re.M|re.I)
						res = obj.group(2)
						tp = dp.parse(res.split()[0])
						tmp = tp.astimezone(timezone('US/Pacific'))
						t_sec = tmp.strftime('%s')
						dev.append(int(t_sec))
				devParse.append(dev)
				file.seek(0)
		return devParse
	else:
		return False

'''
# this is for the toa item in the marconi log file
'''
def parserFunctToa(file,devList):

	regex_dev_toa = r'(.*)TOA:(.*)'
	dev1_val = []
	dev2_val = []
	itr = 0;
	if fileExists(file) != -1:
		with open(file,'r') as file:
			for line in file:
				if devList[0] in line and "TOA" in line:
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev1_val.append(int(res.split()[0]))
				elif devList[1] in line and "TOA" in line:
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev2_val.append(int(res.split()[0]))
		return (dev1_val,dev2_val)
	else:
		return False

'''
To plot the mqtt_sub timestamps
'''
def creatTimeList(file,devId,eDevTmst=None):
	regex_dev_sub_time = r'(.*)Msg Time:(.*)'
	parseTmst = []

	with open(file,'r') as fp:
		for devItr in range(len(devId)):
			dev = []
			if eDevTmst is not None:
				val = eDevTmst[devItr][0]
			for line in fp:
				if devId[devItr] in line:
					obj = re.search(regex_dev_sub_time,line,re.M|re.I)
					res = obj.group(2)
					if eDevTmst is not None:
						dev.append((int(res.split()[0])) - val)
					else:
						dev.append((int(res.split()[0])))
			parseTmst.append(dev)
			fp.seek(0)
	return parseTmst

def plot(filePath,devId,eDevTmst,yax,SF):
	retFlag = 0
	retFlag += fileExists(filePath)

	if retFlag < 1:
		return -1


	tmst = creatTimeList(filePath,devId,eDevTmst)

	# print("Delta Time between End Dev1[fe37] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev1[:20]))
	# print("Delta Time between End Dev2[fe36] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev2[:20]))
	y_axis = []
	colorList = ['r','r','r','r']
	plotCList = ['m','m','m','m']
	mList = ["o","v","s","3"]

	tmstnp = []
	for devItr in range(len(devId)):
		tmptmst = np.array(tmst[devItr])
		tmp = np.array([yax[devItr] for i in range(len(tmst[devItr]))])
		y_axis.append(tmp)
		tmstnp.append(tmptmst)

		# lb1 = 'mqtt sub Dev:..{}'.format(devId[devItr][18:23]) + ' ' + SF[devItr]
		lb1 = 'mqtt sub Dev:{}'.format(devItr+1) + '@' + SF[devItr]

		plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
		plt.plot(tmstnp[devItr][:20], y_axis[devItr][:20], color=plotCList[devItr], linestyle='None', markersize = 10.0)
		plt.scatter(tmstnp[devItr][:20],y_axis[devItr][:20],label = lb1 ,color=colorList[devItr],marker=mList[devItr])

	return tmstnp


def helper(file,devId,thToA,SF):
	"""
	plot various subplots for tx time vs delay in rx
	"""

	eDevTmst = parserEndDev(file[2],devId)
	gwTmst = parserFunct(file[0],devId)
	aServTmst = creatTimeList(file[1],devId)
	totalDev = range(len(devId))

	#pg tmst eDev
	thToaTmst = []
	for devItr in totalDev:
		tmp = []
		thToaVal = thToA[SF[devItr]]/1000
		for itr in range(len(eDevTmst[devItr])):
			tmp.append(eDevTmst[devItr][itr] + thToaVal)
		thToaTmst.append(tmp)

	thProg = []
	for devItr in totalDev:
		tmp = []
		for itr in range(len(thToaTmst[devItr])):
			tmp.append(thToaTmst[devItr][itr] - eDevTmst[devItr][itr])
		thProg.append(tmp)

	gwProg = []
	for devItr in totalDev:
		tmp = []
		for itr in range(len(gwTmst[devItr])):
			tmp.append(gwTmst[devItr][itr] - eDevTmst[devItr][itr])
		gwProg.append(tmp)

	asProg = []
	for devItr in totalDev:
		tmp = []
		for itr in range(len(aServTmst[devItr])):
			tmp.append(aServTmst[devItr][itr] - eDevTmst[devItr][itr])
		asProg.append(tmp)

	lb = ["Th. ToA Tmst in sec","GW Rx Tmst in sec","MQTT sub Tmst in sec"]

	mk = [["o","v","s","3"],['+','*',"x","2"],["8","^",'1','D']]
	c = ['b','m','g','r','c']
	yax = [thProg,gwProg,asProg]

	fig = plt.figure(4)
	ax = plt.subplot(444)

	for pItr in range(3):
		y = yax[pItr]
		cl = c[pItr]
		m = mk[pItr]
		print("totalDev:{}".format(totalDev))
		for itr in totalDev:
			plt.subplot(4,1,itr+1)

			if pItr == 2:
				plt.plot(np.array(eDevTmst[itr][:20]),np.array(y[itr][:20]),'r--',label=lb[pItr],marker=m[itr], color=cl)
			else:
				plt.plot(np.array(eDevTmst[itr][:20]),np.array(y[itr][:20]),'r--',marker=m[itr], color=cl)

			if pItr == 2:
				# print("hi")
				plt.rcParams.update({'font.size': 18,'font.weight': 'bold','font.family':'monospace'})
				plt.title("Dev:{} @ {}".format(itr+1,SF[itr]))
				# # Shrink current axis by 20%
				# box = ax.get_position()
				# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
				#
				# # Put a legend to the right of the current axis
				# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
				# plt.gca().legend(("Th. ToA Tmst Delay in sec","GW Rx Tmst Delay in sec","MQTT sub Tmst Delay in sec"),loc='center left', bbox_to_anchor=(1, 0.5))
				# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
				plt.grid(True)
	# plt.rcParams.update({'font.size': 16})
	font = {'family' : 'monospace','weight' : 'bold','size': 18}
	plt.rc('font',**font)
	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	# plt.gca().yaxis.set_major_locator(plt.NullLocator())
	plt.gca().legend(("Th. ToA Tmst Delay in sec","GW Rx Tmst Delay in sec","MQTT sub Tmst Delay in sec"),loc='center left', bbox_to_anchor=(1, 0.5))
	# ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	plt.grid(True)

	plt.ylabel("Delay in seconds",fontweight='bold',fontsize='14')
	plt.xlabel("Timestamps in seconds from Epoch",fontweight='bold',fontsize='14')
	plt.rcParams.update({'font.size': 18,'font.weight': 'bold','font.family':'monospace'})
	fig.suptitle('Tx Time vs Rx Delay plot for all the end devices')
	plt.rcParams.update({'font.size': 18,'font.weight': 'bold','font.family':'monospace'})
	text_str = 'The plot represents a four subplots for different devices showing the delay between\nthe end dev tx tmst vs rx at gateway vs rx application server measured in seconds\n'
	fig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')




	# plt.gca().yaxis.set_major_locator(plt.NullLocator())
	plt.show()

	# plt.gca()
	# plt.show()


if __name__ == '__main__':

	# filePath1 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_30sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_30sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_30sec.txt']
	filePath1 = ['/home/iot/Desktop/thesisRepoBitBucket/test0930/gwTmstLog0930_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/logMqttSub0930_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/endDevTmstLog0930_15sec.txt']
	# filePath1 = ['/home/iot/Desktop/thesisRepoBitBucket/test0930/gwTmstLog0930.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/logMqttSub0930.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/endDevTmstLog0930.txt']

	devId = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36','00-80-00-00-04-00-98-02','00-80-00-00-04-00-98-2b']
	SF = ['SF8','SF7','SF9','SF10']

	thToA = {'SF10':206.847992,'SF9':308.223999,'SF8':358.912018,'SF7':374.016022,'SF8BW500':163.968002}

	fig = plt.figure(1)
	ax = plt.subplot(111)

	endDevTmst = plotEndDevTmst(filePath1[2],[4,8.5,13,17.5],[3,7.5,12,16.5],SF,thToA,devId)
	mqttSubTmst = plot(filePath1[1],devId,endDevTmst[0],[1,5.5,10,14.5],SF) # mqtt sub/pub plot
	gwTmst = plot_predicted(devId,filePath1[0],endDevTmst[0],[2,6.5,11,15.5],SF) # gw Rx

	# endDevTmst = plotEndDevTmst(filePath1[2],[4,10,14,18],[3,9,13,17],SF,thToA,devId)
	# mqttSubTmst = plot(filePath1[1],devId,endDevTmst[0],[1,7,11,15],SF) # mqtt sub/pub plot
	# gwTmst = plot_predicted(devId,filePath1[0],endDevTmst[0],[2,8,12,16],SF) # gw Rx

	plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	plt.title('End Dev Tx vs Gateway Rx Time vs mqtt sub(marconi) time for 4 End Dev Transmitting max PL operating at Data Rates [DR0,DR1,DR2 & DR3]')
	plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
	text_str = 'Data Transfer Path: mdot Tx -> Gateway Rx -> Gateway MQTT pub -> marconi MQTT sub\nThe above graph is the representation of 1st 20 tmst. from start of Tx from End Dev\n'
	fig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')

	# Shrink current axis by 20%
	font = {'family' : 'monospace','weight' : 'bold','size': 16}
	plt.rc('font',**font)
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


	plt.gca().yaxis.set_major_locator (plt.NullLocator())
	plt.grid(True)
	plt.show()

	# deltaTime,std = getMeanStd(endDevTmst[0],gwTmst[0],devId)
	timeDiff = interArrivalTimeDiff(filePath1[0], devId,SF)
	# print("time Diff:\n{}\n".format(timeDiff))

	# plot subplots for tx time vs delay in rx for different end dev
	helper(filePath1,devId,thToA,SF)
