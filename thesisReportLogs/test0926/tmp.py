import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import re
import time, datetime, pytz
from pytz import timezone
import dateutil.parser as dp
from statistics import mean,stdev
import queue

from math import ceil,floor

"""
Plot the mmc Queue simulation for the inter arrival times for our test bed
"""
def interArrivalTimeDiff(file, devId,SF,axis1,cr):
	diffTimes = []
	gwRxTmst = []
	totalDev = len(devId)

	arrTime =  parserFunct(file,devId)
	servTime =  parseServTmst(file,devId)
	arrRate = []
	servRate = []
	interArrTimes = []
	aTime = []
	sTime = []
	rho = []
	ls = []
	waitTime = []
	totalEntities = []
	timeHours = 6

	wTime = []
	tAt = []
	tSt = []
	waitList = []
	yax = []

	print("\n")
	for devItr in range(totalDev):
		total = len(arrTime[devItr])
		print("devId:{} total:{}\n".format(devId[devItr],total))
		tm1 = []
		tm2 = []
		for itr in range(total):
			arrTime[devItr][itr] = int(arrTime[devItr][itr])
			servTime[devItr][itr] = int(servTime[devItr][itr])
			tm1.append(arrTime[devItr][itr] - arrTime[devItr][0])
			tm2.append(servTime[devItr][itr] - servTime[devItr][0])
		aTime.append(tm1)
		sTime.append(tm2)

		diffArr = arrTime[devItr][-1] - arrTime[devItr][0]
		diffToa = servTime[devItr][-1] - servTime[devItr][0]
		print("diffArr:{}\ndiffToa:{}\n".format(diffArr,diffToa))

		diffArr /= (3600)
		diffToa /= (3600)
		print("....diffArr:{}\ndiffToa:{}\n".format(diffArr,diffToa))

		arrRate.append(total/diffArr)
		servRate.append(total/diffToa)

		rho.append(float(floor(arrRate[devItr])/ceil(servRate[devItr])))
		arrRate[devItr] = floor(arrRate[devItr])
		servRate[devItr] = ceil(servRate[devItr])

		tmpLs = float(rho[devItr]/(1-rho[devItr]))
		ls.append(tmpLs)
		tmpWs = tmpLs/arrRate[devItr]
		waitTime.append(tmpWs)
		print("Dev:{} => rho (arrRate/servRate):{} Ls:{} Ws(waitTime):{}\n".format(devId[devItr],rho[devItr],tmpLs,tmpWs))
		totalEntities.append(int(np.random.poisson(arrRate[devItr])*timeHours))

	print("(servRate):{}\n(ls):{}\n(arrRate):{}\n".format(np.array(servRate),np.array(ls),np.array(arrRate)))

	# fig_upbound = plt.figure(2)
	# fig = plt.figure(3)

	# sfig, ax = plt.subplots()

	# plt.subplot(1,2,1)
	# lm_mu = [np.array(arrRate),np.array(servRate)]
	# ln = ['--',':']
	# lb = ["lambda","mu"]
	# mk = ['*','p']
	# cr = ['k','b']
	#
	# for tpItr in range(2):
	# 	plt.plot(lm_mu[tpItr],np.array(ls),ln[tpItr],label=lb[tpItr],marker=mk[tpItr], color=cr[tpItr])
	#
	# 	# plt.plot(lm_mu[tpItr],np.array(ls),'--',label="lambda",marker='*', color='k')
	# 	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# 	# plt.title("Arrival Rate vs Expected Number of Packets in the System")
	# 	# plt.ylabel("Ls - Expected Number of Packets in the System",fontweight='bold',fontsize='14')
	# 	# plt.xlabel("Lambda - Arrival Rate [# of packets arrived/hr.]",fontweight='bold',fontsize='14')
	# 	# font = {'family' : 'monospace','weight' : 'bold','size': 14}
	# 	# plt.rc('font',**font)
	# 	# # Shrink current axis by 20%
	# 	# box = ax.get_position()
	# 	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	# 	# # Put a legend to the right of the current axis
	# 	# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# 	# plt.gca().legend(loc='best', bbox_to_anchor=(1, 0.5))
	# 	# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# 	# plt.grid(True)
	#
	# 	# plt.subplot(1,2,2)
	# 	# plt.plot(np.array(servRate),np.array(ls),':',label="mu",marker='p', color='b')
	# 	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# 	# plt.title("Service Rate vs Expected Number of Packets in the System")
	# 	# plt.ylabel("Ls - Expected Number of Packets in the System",fontweight='bold',fontsize='14')
	# 	# plt.xlabel("Mu - Service Rate [# of packets serviced/hr.]",fontweight='bold',fontsize='14')
	# 	# font = {'family' : 'monospace','weight' : 'bold','size': 14}
	# 	# plt.rc('font',**font)
	# 	# # Shrink current axis by 20%
	# 	# box = ax.get_position()
	# 	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	# 	# # Put a legend to the right of the current axis
	# 	# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# 	# plt.gca().legend(loc='best', bbox_to_anchor=(1, 0.5))
	# 	# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# 	# plt.grid(True)
	#
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# plt.title("Arrival Rate vs Expected Number of Packets in the System")
	# plt.ylabel("Ls - Expected Number of Packets in the System",fontweight='bold',fontsize='14')
	# plt.xlabel("Lambda - Arrival Rate [# of packets arrived/hr.] and\nMu - Service Rate [# of packets serviced/hr]",fontweight='bold',fontsize='14')
	# font = {'family' : 'monospace','weight' : 'bold','size': 14}
	# plt.rc('font',**font)
	# # Shrink current axis by 20%
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
	# # Put a legend to the right of the current axis
	# # plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# plt.gca().legend(loc='best', bbox_to_anchor=(1, 0.5))
	# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	# plt.grid(True)
	#
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# # fig_upbound.suptitle('Lambda - Arrival Rate and Mu - Service Rate represented as\nfunction of Ls - Expected # of packets in the system')
	# fig.suptitle('Lambda - Arrival Rate and Mu - Service Rate represented as\nfunction of Ls - Expected # of packets in the system')
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# text_str = 'The plots represents 1) The upper bond for end device UpLink Rate transmitting at [SF7,SF8,SF9,SF10] by showing Lambda and Mu as function of Ls in plot 1\n2)Total estimated packets serviced by the system using M/M/1:inf.:inf. Queuing Model in plot2'
	# # fig_upbound.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
	# fig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')

	"""
	Compute inter arrival times and arrival time
	"""
	for devItr in range(totalDev):
		iat = []
		at = []
		st = []
		wt = []
		for entities in range(totalEntities[devItr]):
			val1 = np.random.exponential(1/arrRate[devItr])*(60*60)
			val2 = np.random.exponential(1/servRate[devItr])*(60*60)
			st.append(val2)

			if entities != 0:
				iat.append(int(val1))
				at.append(at[entities-1]+iat[entities])
				wt.append(at[entities] + waitTime[devItr])
			else:
				iat.append(0)
				at.append(0)
				wt.append(0)

		interArrTimes.append(iat)
		tAt.append(at)
		tSt.append(st)
		waitList.append(wt)

	"""
	queue sim
	"""
	# fig = plt.figure(3)
	# plt.subplot(1,2,2)
	# ax2 = None
	for devItr in range(totalDev):
		cp = None
		en = aTime[devItr][0]
		itr = 0
		ytmp = []
		serviced = 0
		serv = False
		mm1Queue = queue.Queue()

		while ((itr < len(aTime[devItr])) and (en < (timeHours*60*60))):
			if serv and (cp < len(tSt[devItr])):
				tSt[devItr][cp] -= 1
				if tSt[devItr][cp] <= 0:
					serv = False
					serviced += 1

			mm1Queue.put(en)

			if not serv and not mm1Queue.empty():
				cp = mm1Queue.get()
				serv = True

			sw = 0

			if serviced == 0:
				ytmp.append(0)
			else:
				for ens in range(serviced):
					sw += waitList[devItr][ens]
				ytmp.append(sw/(serviced*60*60))

			itr += 1
			if ((itr < len(aTime[devItr]))):
				en = aTime[devItr][itr]

		yax.append(ytmp)

		lim = None
		if totalEntities[devItr] > len(yax[devItr]):
			lim = len(yax[devItr])
		else:
			lim = totalEntities[devItr]

		lb = "Dev:{} SF:{}".format(devItr+1,SF[devItr])

		xax = [(tmp+1) for tmp in range(lim)]
		axis1.plot(xax,yax[devItr][:lim],color=cr[devItr])

		# print(yax[devItr][:lim])
    #------------------------
    # ax2 = ax.twinx()
	# # ax2.plot(xax,rho[devItr])
	# ax2.plot(np.array(ls),np.array(rho),'ro',label='rho')
	# devItr = 0
	# for xy in zip(ls,rho):
	# 	plt.rcParams.update({'font.size': 10,'font.weight': 'bold','font.family':'monospace'})
	# 	ax2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')
	# 	devItr += 1
    #
	# print("rho:{}".format(rho))
    # -----------------------

	# for devItr in range(totalDev):
	# 	for xy in zip(ls,rho):
	# 		ax2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')

	# sfig.tight_layout()
	# plt.ylabel("Average Metering Payload Delay[seconds]",fontsize=14,fontweight='bold')


    # -------------------------
    # ax.set_ylabel("Average Metering Payload Delay[seconds]",fontsize=16,fontweight='bold')
	# ax.set_xlabel("Total LoRa packets In The System\n[LoRa Gateway + Application Server]",fontsize=14,fontweight='bold')
	# ax2.set_ylabel("rho (arrRate[lambda]/servRate[mu]) - Utilization Factor",fontsize=16,fontweight='bold')
    #
	# font = {'family' : 'monospace','weight' : 'bold','size': 16}
	# plt.rc('font',**font)
	# plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
	# plt.title('# of LoRa packets in the system vs Avg. Metering Payload Delay[seconds] simulated using M/M/1 queue\nUtilization Factor - rho vs Avg. Metering Payload Delay[seconds]')
    #
	# # ax = plt.subplot(111)
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    #
	# box1 = ax2.get_position()
	# ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    #
	# # Put a legend to the right of the current axis
	# ax.legend(loc='best', bbox_to_anchor=(1, 0.5))
	# ax2.legend(loc='best', bbox_to_anchor=(1, 0.25))
	# # plt.gca().yaxis.set_major_locator (plt.NullLocator())
	# # text_str = 'The plots represents 1)Total estimated packets serviced by the system using M/M/1:inf.:inf. Queuing Model with left y-axis\n2)) The upper bond for end device UpLink Rate transmitting at [SF8,SF7,SF9,SF10] by showing rho < 1 as function of Ls in plot with right y-axis\n'
	# # # fig_upbound.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
	# # sfig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
	# plt.grid(True)
	# plt.show()

	ret = [arrRate,servRate,rho,ls,waitTime]
	return ret

def fileExists(file):
    if os.path.exists(file):
        print("file:%s found\n"%file)
        return 1
    else:
        print("File:%s not found\n"%file)
        return -1

def parseServTmst(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)mqtt Tx:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						res = obj.group(2);
						t_sec = res.split()[0]
						dev.append(int(t_sec))

				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False

def parserFunct(file,devId):
	devREx = r'(.*)Device ID:(.*)'
	mtimeRegEx = r'(.*)Msg Time: (.*)'

	dev2 = []
	dev1 = []
	devParse = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(mtimeRegEx,line,re.M|re.I)
						res = obj.group(2)
						tp = dp.parse(res.split()[0])
						tmp = tp.astimezone(timezone('US/Pacific'))
						t_sec = tmp.strftime('%s')
						dev.append(int(t_sec))
				devParse.append(dev)
				file.seek(0)
		return devParse
	else:
		return False

if __name__ == '__main__':
    filePath1 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_30sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_30sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_30sec.txt']
    filePath2 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_15sec.txt']
    filePath3 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_10sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_10sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_10sec.txt']
    filePath4 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926.txt']
    filePath5 = ['/home/iot/Desktop/thesisRepoBitBucket/test0720/gwTmstLog0711.txt','/home/iot/Desktop/thesisRepoBitBucket/test0720/logMqttSub0711.txt','/home/iot/Desktop/thesisRepoBitBucket/test0720/endDevTmstLog0711.txt']

    fileList = [filePath1,filePath2,filePath3,filePath4,filePath5]
    clr = ['r','g','b','y','m']
    cr = ["orange",'b','g','r']
    mk = ["8","*","o","x","2"]
    fq = [[0.033,0.033,0.033,0.033],[0.067,0.067,0.067,0.067],[0.1,0.1,0.1,0.1],[1,1,1,1],[0.2,0.2,0.2,0.2]]

    devId = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36','00-80-00-00-04-00-98-02','00-80-00-00-04-00-98-2b']
    SF = ['SF8','SF7','SF9','SF10']
    thToA = {'SF10':206.847992,'SF9':308.223999,'SF8':358.912018,'SF7':374.016022,'SF8BW500':163.968002}

    # sfig, ax = plt.subplots()
    fig = plt.figure(1)
    axis1 = fig.add_subplot(111,label = "1")
    axis2 = fig.add_subplot(111,label = "2",frame_on = False)

    # ax2 = None
    arrRate,servRate,rho,ls,waitTime = None, None, None, None, None
    for itr in range(len(fileList)):
        # arrRate,servRate,rho,ls,waitTime = interArrivalTimeDiff(filePath1[0], devId,SF,sfig,ax)
        arrRate,servRate,rho,ls,waitTime = interArrivalTimeDiff(filePath1[0], devId,SF,axis1,cr)
        # ax2 = ax.twinx()
    	# ax2.plot(xax,rho[devItr])
        # ax2.plot(np.array(fq[itr]),np.array(rho),'k--',label='rho',marker=mk[itr],color=clr[itr])
        axis2.plot(np.array(fq[itr]),np.array(rho),'k--',label='rho',marker=mk[itr],color=clr[itr])
        devItr = 0
        for xy in zip(ls,rho):
            plt.rcParams.update({'font.size': 10,'font.weight': 'bold','font.family':'monospace'})
            # ax2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')
            axis2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')
            devItr += 1

        if itr == 3:
            axis1.set_ylabel("Average Metering Payload Delay[seconds]",fontsize=16,fontweight='bold')
            axis1.set_xlabel("Total LoRa packets In The System\n[LoRa Gateway + Application Server]",fontsize=14,fontweight='bold')
            axis1.tick_params(axis='x')
            axis1.tick_params(axis='y')
            axis2.set_ylabel("rho (arrRate[lambda]/servRate[mu]) - Utilization Factor",fontsize=16,fontweight='bold')
            axis2.set_xlabel("Measured uplink frequency [packets/sec]",fontsize=14,fontweight='bold')
            axis2.tick_params(axis='x')
            axis2.tick_params(axis='y')
            plt.show()

        print("rho:{}".format(rho))

    # ax.set_ylabel("Average Metering Payload Delay[seconds]",fontsize=16,fontweight='bold')
    # ax.set_xlabel("Total LoRa packets In The System\n[LoRa Gateway + Application Server]",fontsize=14,fontweight='bold')
    #
    # ax2.set_ylabel("rho (arrRate[lambda]/servRate[mu]) - Utilization Factor",fontsize=16,fontweight='bold')
    # ax2.set_xlabel("Measured uplink frequency [packets/sec]")

    # axis1.set_ylabel("Average Metering Payload Delay[seconds]",fontsize=16,fontweight='bold')
    # axis1.set_xlabel("Total LoRa packets In The System\n[LoRa Gateway + Application Server]",fontsize=14,fontweight='bold')
    #
    # axis2.set_ylabel("rho (arrRate[lambda]/servRate[mu]) - Utilization Factor",fontsize=16,fontweight='bold')
    # axis2.set_xlabel("Measured uplink frequency [packets/sec]")

    font = {'family' : 'monospace','weight' : 'bold','size': 16}
    plt.rc('font',**font)
    plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
    plt.title('# of LoRa packets in the system vs Avg. Metering Payload Delay[seconds] simulated using M/M/1 queue\nMeasure uplink frequency [pkt./sec] v/s Utilization Factor - rho')

	# ax = plt.subplot(111)
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    #
    # box1 = ax2.get_position()
    # ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    #
    # # Put a legend to the right of the current axis
    # ax.legend(loc='best', bbox_to_anchor=(1, 0.5))
    # ax2.legend(loc='best', bbox_to_anchor=(1, 0.25))


    box1 = axis2.get_position()
    axis2.set_position([box1.x0, box1.y0, box1.width * 0.8, box1.height])

    # Put a legend to the right of the current axis
    axis2.legend(loc='best', bbox_to_anchor=(1, 0.25))

    # plt.gca().yaxis.set_major_locator (plt.NullLocator())
    # text_str = 'The plots represents 1)Total estimated packets serviced by the system using M/M/1:inf.:inf. Queuing Model with left y-axis\n2)) The upper bond for end device UpLink Rate transmitting at [SF8,SF7,SF9,SF10] by showing rho < 1 as function of Ls in plot with right y-axis\n'
    # # fig_upbound.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
    # sfig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
    plt.grid(True)

	# endDevTmst = plotEndDevTmst(filePath1[2],[4,8.5,13,17.5],[3,7.5,12,16.5],SF,thToA,devId)
	# mqttSubTmst = plot(filePath1[1],devId,endDevTmst[0],[1,5.5,10,14.5],SF) # mqtt sub/pub plot
	# gwTmst = plot_predicted(devId,filePath1[0],endDevTmst[0],[2,6.5,11,15.5],SF) # gw Rx
    #
	# # endDevTmst = plotEndDevTmst(filePath1[2],[4,10,14,18],[3,9,13,17],SF,thToA,devId)
	# # mqttSubTmst = plot(filePath1[1],devId,endDevTmst[0],[1,7,11,15],SF) # mqtt sub/pub plot
	# # gwTmst = plot_predicted(devId,filePath1[0],endDevTmst[0],[2,8,12,16],SF) # gw Rx
    #
	# plt.rcParams.update({'font.size': 14,'font.weight': 'bold','font.family':'monospace'})
	# plt.title('End Dev Tx vs Gateway Rx Time vs mqtt sub(marconi) time for 4 End Dev Transmitting max PL operating at Data Rates [DR0,DR1,DR2 & DR3]')
	# plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
	# text_str = 'Data Transfer Path: mdot Tx -> Gateway Rx -> Gateway MQTT pub -> marconi MQTT sub\nThe above graph is the representation of 1st 20 tmst. from start of Tx from End Dev\n'

    text_str = 'The plots represents 1)Total estimated packets serviced by the system using M/M/1:inf.:inf. Queuing Model with left y-axis\n2) Measured UpLink Rate frequency[Packets/seconds] transmitting at [SF8,SF7,SF9,SF10] on upper x-axis v/s rho < 1 as function of Ls in plot with right y-axis\n'
	# fig_upbound.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')
    # sfig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')

    fig.text(.5,0.01,text_str,wrap=True,ha='center',weight='bold')

	# Shrink current axis by 20%
    font = {'family' : 'monospace','weight' : 'bold','size': 16}
    plt.rc('font',**font)
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	# ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


	# plt.gca().yaxis.set_major_locator (plt.NullLocator())
    # plt.grid(True)
    plt.show()

	# deltaTime,std = getMeanStd(endDevTmst[0],gwTmst[0],devId)
	# print("time Diff:\n{}\n".format(timeDiff))

	# plot subplots for tx time vs delay in rx for different end dev
	# helper(filePath1,devId,thToA,SF)
