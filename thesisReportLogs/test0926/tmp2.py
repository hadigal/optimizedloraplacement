import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import re
import time, datetime, pytz
from pytz import timezone
import dateutil.parser as dp
from statistics import mean,stdev
import queue

from math import ceil,floor

"""
File check
"""
# def checkFile(path):
#     if os.path.exists(path):
#         print("Found file:%s"%path)
#         return True
#     else:
#         print("File not found\n")
#         return False

"""
Plot the mmc Queue simulation for the inter arrival times for our test bed
"""
def interArrivalTimeDiff(file,devId,SF,ax,cr):
    diffTimes = []
    gwRxTmst = []
    totalDev = len(devId)

    arrTime =  parserFunct(file,devId)
    servTime =  parseServTmst(file,devId)
    # servTime = creatTimeList(file[1],devId)
    print("sev:{}\n{}\n{}\n{}\naT:{}\n".format(servTime[0][:10],servTime[1][:10],servTime[2][:10],servTime[3][:10],arrTime[0][:2]))
    arrRate = []
    servRate = []
    interArrTimes = []
    aTime = []
    sTime = []
    rho = []
    ls = []
    waitTime = []
    totalEntities = []
    timeHours = 6

    wTime = []
    tAt = []
    tSt = []
    waitList = []
    yax = []

    print("\n")
    for devItr in range(totalDev):
        total = len(arrTime[devItr])
        # total = 100
        print("devId:{} total:{}\n".format(devId[devItr],total))
        tm1 = []
        tm2 = []
        for itr in range(total):
            arrTime[devItr][itr] = int(arrTime[devItr][itr])
            servTime[devItr][itr] = int(servTime[devItr][itr])
            tm1.append(arrTime[devItr][itr] - arrTime[devItr][0])
            tm2.append(servTime[devItr][itr] - servTime[devItr][0])
        aTime.append(tm1)
        sTime.append(tm2)

        diffArr = arrTime[devItr][-1] - arrTime[devItr][0]
        diffToa = servTime[devItr][-1] - servTime[devItr][0]
        # diffArr = arrTime[devItr][99] - arrTime[devItr][0]
        # diffToa = servTime[devItr][99] - servTime[devItr][0]
        print("diffArr:{}\ndiffToa:{}\n".format(diffArr,diffToa))

        diffArr /= (3600)
        diffToa /= (3600)

        print("....diffArr:{}\ndiffToa:{}\n".format(diffArr,diffToa))

        arrRate.append(total/diffArr)
        servRate.append(total/diffToa)

        print("1)----->arr_rate: {}\n2)---->serv_rate:{}\n".format(arrRate[devItr],servRate[devItr]))

        rho.append(float(floor(arrRate[devItr])/ceil(servRate[devItr])))
        # rho.append(float(arrRate[devItr]/servRate[devItr]))
        print("rho=========:{}\n".format(rho[devItr]))
        arrRate[devItr] = floor(arrRate[devItr])
        servRate[devItr] = ceil(servRate[devItr])

        tmpLs = float(rho[devItr]/(1-rho[devItr]))
        ls.append(tmpLs)
        tmpWs = tmpLs/arrRate[devItr]
        waitTime.append(tmpWs)
        print("Dev:{} => rho (arrRate/servRate):{} Ls:{} Ws(waitTime):{}\n".format(devId[devItr],rho[devItr],tmpLs,tmpWs))
        totalEntities.append(int(np.random.poisson(arrRate[devItr])*timeHours))

    # print("(servRate):{}\n(ls):{}\n(arrRate):{}\n".format(np.array(servRate),np.array(ls),np.array(arrRate)))

    """
    Compute inter arrival times and arrival time
    """
    for devItr in range(totalDev):
        iat = []
        at = []
        st = []
        wt = []
        for entities in range(totalEntities[devItr]):
            val1 = np.random.exponential(1/arrRate[devItr])*(60*60)
            val2 = np.random.exponential(1/servRate[devItr])*(60*60)
            st.append(val2)

            if entities != 0:
                iat.append(int(val1))
                at.append(at[entities-1]+iat[entities])
                wt.append(at[entities] + waitTime[devItr])
            else:
                iat.append(0)
                at.append(0)
                wt.append(0)

        interArrTimes.append(iat)
        tAt.append(at)
        tSt.append(st)
        waitList.append(wt)

    """
    queue sim
    """
    for devItr in range(totalDev):
        cp = None
        en = aTime[devItr][0]
        itr = 0
        ytmp = []
        serviced = 0
        serv = False
        mm1Queue = queue.Queue()

        while ((itr < len(aTime[devItr])) and (en < (timeHours*60*60))):
            if serv and (cp < len(tSt[devItr])):
                tSt[devItr][cp] -= 1
                if tSt[devItr][cp] <= 0:
                    serv = False
                    serviced += 1

            mm1Queue.put(en)

            if not serv and not mm1Queue.empty():
                cp = mm1Queue.get()
                serv = True

            sw = 0

            if serviced == 0:
                ytmp.append(0)
            else:
                for ens in range(serviced):
                    sw += waitList[devItr][ens]
                ytmp.append(sw/(serviced*60*60))

            itr += 1
            if ((itr < len(aTime[devItr]))):
                en = aTime[devItr][itr]

        yax.append(ytmp)

        lim = None
        if totalEntities[devItr] > len(yax[devItr]):
            lim = len(yax[devItr])
        else:
            lim = totalEntities[devItr]

        lb = "Dev:{} SF:{}".format(devItr+1,SF[devItr])

        xax = [(tmp+1) for tmp in range(lim)]

        plt.rcParams.update({'font.weight': 'bold','font.family':'monospace'})
        ax.plot(xax,yax[devItr][:lim],color=cr[devItr])

    # print("rho:{}".format(rho))
    ret = [arrRate,servRate,rho,ls,waitTime]
    # if not (os.path.exists("/home/iot/Desktop/thesisRepoBitBucket/testLogs/ws_ls.txt")):
    #     with open("/home/iot/Desktop/thesisRepoBitBucket/testLogs/ws_ls.txt", 'w'):
    #         pass

    # with open("/home/iot/Desktop/thesisRepoBitBucket/testLogs/ws_ls.txt","a") as fp:
    #     for devItr in range(totalDev):
    #         fp.write("ls:{}\n--\n".format(SF[devItr]))
    #         fp.write("\n".join(str(ls[devItr])))

    # fp.close
    return ret

def fileExists(file):
    if os.path.exists(file):
        print("file:%s found\n"%file)
        return 1
    else:
        print("File:%s not found\n"%file)
        return -1

def parserFunct(file,devId):
	devREx = r'(.*)Device ID:(.*)'
	mtimeRegEx = r'(.*)Msg Time: (.*)'

	dev2 = []
	dev1 = []
	devParse = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(mtimeRegEx,line,re.M|re.I)
						res = obj.group(2)
						tp = dp.parse(res.split()[0])
						tmp = tp.astimezone(timezone('US/Pacific'))
						t_sec = tmp.strftime('%s')
						dev.append(int(t_sec))
				devParse.append(dev)
				file.seek(0)
		return devParse
	else:
		return False

def parseServTmst(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)mqtt Tx:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						res = obj.group(2);
						t_sec = res.split()[0]
						dev.append(int(t_sec))

				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False

'''
To plot the mqtt_sub timestamps
'''
def creatTimeList(file,devId,eDevTmst=None):
	regex_dev_sub_time = r'(.*)Msg Time:(.*)'
	parseTmst = []

	with open(file,'r') as fp:
		for devItr in range(len(devId)):
			dev = []
			if eDevTmst is not None:
				val = eDevTmst[devItr][0]
			for line in fp:
				if devId[devItr] in line:
					obj = re.search(regex_dev_sub_time,line,re.M|re.I)
					res = obj.group(2)
					if eDevTmst is not None:
						dev.append((int(res.split()[0])) - val)
					else:
						dev.append((int(res.split()[0])))
			parseTmst.append(dev)
			fp.seek(0)
	return parseTmst

if __name__ == '__main__':

    filePath1 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_30sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_30sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_30sec.txt']
    # filePath2 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_15sec.txt']
    filePath2 = ['/home/iot/Desktop/thesisRepoBitBucket/test0930/gwTmstLog0930_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/logMqttSub0930_15sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/endDevTmstLog0930_15sec.txt']
    filePath3 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926_10sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926_10sec.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926_10sec.txt']
    # filePath4 = ['/home/iot/Desktop/thesisRepoBitBucket/test0926/gwTmstLog0926.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/logMqttSub0926.txt','/home/iot/Desktop/thesisRepoBitBucket/test0926/endDevTmstLog0926.txt']
    filePath4 = ['/home/iot/Desktop/thesisRepoBitBucket/test0930/gwTmstLog0930.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/logMqttSub0930.txt','/home/iot/Desktop/thesisRepoBitBucket/test0930/endDevTmstLog0930.txt']
    filePath5 = ['/home/iot/Desktop/thesisRepoBitBucket/test0720/gwTmstLog0711.txt','/home/iot/Desktop/thesisRepoBitBucket/test0720/logMqttSub0711.txt','/home/iot/Desktop/thesisRepoBitBucket/test0720/endDevTmstLog0711.txt']

    fList = [filePath1,filePath2,filePath3,filePath4,filePath5]
    devId = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36','00-80-00-00-04-00-98-02','00-80-00-00-04-00-98-2b']
    SF = ['SF8','SF7','SF9','SF10']
    # fq = [[2,2,2,2],[4,4,4,4],[6,6,6,6],[40,40,40,40],[12,12,12,12]]
    fq = [[0.033,0.033,0.033,0.033],[0.067,0.067,0.067,0.067],[0.1,0.1,0.1,0.1],[1,1,1,1],[0.2,0.2,0.2,0.2]]

    clr = ['r','g','b','y','m']
    mk = ["8","*","o","x","2"]
    cr = ["darkorange","steelblue",'g',"firebrick"]

    thToA = {'SF10':206.847992,'SF9':308.223999,'SF8':358.912018,'SF7':374.016022,'SF8BW500':163.968002}

    sfig, ax = plt.subplots()
    ax2 = None
    for itr in range(len(fList)):
        # arrRate,servRate,rho,ls,waitTime = interArrivalTimeDiff([fList[itr][0],fList[itr][1]], devId,SF,ax,cr)
        arrRate,servRate,rho,ls,waitTime = interArrivalTimeDiff(fList[itr][0], devId,SF,ax,cr)
        ax3 = ax.twiny()
        if itr == 4:
            ax3.set_xlabel("Measured UpLink frequency [pkt/sec]",fontsize=16,fontweight='bold')

        ax2 = ax.twinx()
    	# ax2.plot(xax,rho[devItr])
    	# ax2.plot(np.array(fq[itr]),np.array(rho),'ro',label='rho')
        print(".....rho:\n{}\n...ls:\n{}\n...Ws:\n{}\n".format(rho,ls,waitTime))
        plt.rcParams.update({'font.weight': 'bold','font.family':'monospace'})
        ax2.plot(np.array(fq[itr]),np.array(rho),'k--',label='freq. vs rho',marker='o')
        if itr == 4:
            devItr = 0
            for xy in zip(fq[itr],rho):
                plt.rcParams.update({'font.size': 10,'font.weight': 'bold','font.family':'monospace'})
                ax2.annotate('{}'.format(SF[devItr]),xy=xy,textcoords='data')
                devItr += 1

    ax.set_ylabel("Average Metering Payload Delay[seconds]",fontsize=16,fontweight='bold')
    ax.set_xlabel("Total LoRa packets In The System\n[LoRa Gateway + Application Server]",fontsize=14,fontweight='bold')
    ax2.set_ylabel("rho (arrRate[lambda]/servRate[mu]) - Utilization Factor",fontsize=16,fontweight='bold')

    font = {'family' : 'monospace','weight' : 'bold','size': 16}
    plt.rc('font',**font)
    plt.rcParams.update({'font.size': 16,'font.weight': 'bold','font.family':'monospace'})
    plt.title('# of LoRa packets in the system vs Avg. Metering Payload Delay[seconds] simulated using M/M/1 queue\nMeasured UpLink frequency vs Utilization Factor - rho')

    box1 = ax2.get_position()
    ax2.set_position([box1.x0, box1.y0, box1.width * 0.8, box1.height])

	# Put a legend to the right of the current axis
    ax2.legend(loc='best', bbox_to_anchor=(1, 0.25))

    plt.grid(True)
    plt.show()
