import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import re
import time, datetime, pytz
from pytz import timezone
import dateutil.parser as dp
from statistics import mean,stdev

"""
File check
"""
# def checkFile(path):
#     if os.path.exists(path):
#         print("Found file:%s"%path)
#         return True
#     else:
#         print("File not found\n")
#         return False


def interArrivalTimeDiff(file, devId):
	diffTimes = []
	gwRxTmst = []

	arrTime =  parserFunct(file,devId)
	servTime =  parseServTmst(file,devId)
	arrRate = []
	servRate = []

	for devItr in range(len(devId)):
		total = len(arrTime[devItr])
		# print("total:{}\n".format(total))
		for itr in range(total):
			arrTime[devItr][itr] = int(arrTime[devItr][itr])
			servTime[devItr][itr] = round((int(servTime[devItr][itr]))/10)

		diffArr = arrTime[devItr][-1] - arrTime[devItr][0]
		diffToa = servTime[devItr][-1] - servTime[devItr][0]
		# print("diffAir:{}\ndiffToa:{}\n".format(diffArr,diffToa))

		diffArr /= (3600)
		diffToa /= (3600)

		arrRate.append(total/diffArr)
		servRate.append(total/diffToa)
		print("dev:{} => arrival rate:{} and service rate:{}\n".format(devId[devItr],arrRate[devItr],servRate[devItr]))

	# print("server_tmst:{}\n".format(servTime))
	# ret = [arrRate,servRate,arrTime,servTime]
	ret = [arrRate,servRate]

	# print("dev1:{}\ndev2:{}\n".format(dev1,dev2))
	# gwRxTmst.append(dev1)
	# gwRxTmst.append(dev2)
	#
	# for itr1 in range(total):
	# 	tmp = []
	# 	prev = 0;
	# 	for itr2 in range(len(gwRxTmst[itr1])):
	# 		gwRxTmst[itr1][itr2] = int(gwRxTmst[itr1][itr2])
	# 		if itr2 == 0:
	# 			#prev = gwRxTmst[itr1][itr2]
	# 			tmp.append(0)
	# 		else:
	# 			#gwRxTmst[itr1][itr2] = int(gwRxTmst[itr1][itr2])
	# 			tmp.append(gwRxTmst[itr1][itr2] - prev)
	# 			#prev = gwRxTmst[itr1][itr2]
	# 		prev = gwRxTmst[itr1][itr2]
	#
	# 	diffTimes.append(tmp)


	return ret

"""
Get the mean and standard deviation
"""
def getMeanStd(endDevTmst,serverTmst,devName=['dev1','dev2']):

	gwRx = []

	for devItr in range(len(endDevTmst)):
		deltaT = []
		for itr in range(len(endDevTmst[devItr])):
			delta = serverTmst[devItr][itr] - endDevTmst[devItr][itr]
			deltaT.append(delta)
		gwRx.append(deltaT)

	# print("\nThe mean of gw tmst with respect to end Dev Tx tmst:{}\n".format(gwRx))
	colorList = ['c','b']
	devMean = []
	std = []

	for itr in range(len(devName)):
		devMean.append(mean(gwRx[itr]))
		std.append(stdev(gwRx[itr]))
		print("Dev:{} -> Mean: {}\t Std. Dev.:{}".format(devName[itr],devMean[itr],std[itr]))

	return devMean,std

"""
Plot the gw rx timestamps and toa timestamps
"""
#def plot_predicted(devEUIs,path,tmstDev1,tmstDev2,yax,sf37,sf36):
def plot_predicted(devId,path,tmstEndDev,yax,SF):

	"""
	Gateway RX tmst
	"""
	devTmst = parserFunct(path,devId)
	dTmst = []

	for itr in range(len(devTmst)):
		tmp = []
		for tmst in devTmst[itr]:
			tmp.append(int(tmst))
		dTmst.append(tmp)

	"""
	gw rx progessive tmst for plot
	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	"""
	gTime = []
	for devItr in range(len(dTmst)):
		tmp = []
		for itr in range(len(dTmst[devItr])):
			tmp.append(dTmst[devItr][itr] - tmstEndDev[devItr][0])
		gTime.append(tmp)

	# print("gtime:{}\n".format(gTime))

	"""
	y axis for plot
	"""
	y_axis = []
	for devItr in range(len(gTime)):
		tmp = np.array([yax[devItr] for i in range(len(gTime[devItr][:20]))])
		y_axis.append(tmp)

	"""
	Compute mean and std for each device
	"""
	devMean,std = getMeanStd(tmstEndDev,dTmst,devId)

	"""
	Plot
	"""
	colorList = ['g','b']
	markerList = ['+','*']

	for itr in range(len(devId)):
		lb = "GW Rx(UpLink) Dev:..{}".format(devId[itr][17:23]) + ' ' + SF[itr]+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[itr])+'\n'+'Std. Dev:'+"{:.3f}".format(std[itr])
		plt.rcParams.update({'font.size': 11})
		plt.scatter(gTime[itr][:20],y_axis[itr], label=lb, color=colorList[itr],marker=markerList[itr])

	plt.rcParams.update({'font.size': 14})
	plt.xlabel('Time in secs')
	plt.ylabel('DR0 vs DR3')

	return dTmst

def plotEndDevTmst(file,yax,toaYax,SF,thToA,devId):

	"""
	# end dev tx
	"""
	parseTime = parserEndDev(file,devId)

	tmst = []
	for devItr in range(len(parseTime)):
		tmp = []
		for tm in parseTime[devItr]:
			tmp.append(int(tm))
		tmst.append(tmp)

	# print("tmst:{}".format(tmst))

	"""
	progessive Tx tmst end dev
	"""
	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	eDevTmst = []
	toaTmst = []
	for devItr in range(len(tmst)):
		tmp = []
		toaTmp = []
		val = tmst[devItr][0]
		thToaVal = thToA[SF[devItr]]/1000
		for itr in range(len(tmst[devItr])):
			tmp.append(tmst[devItr][itr] - val)
			toaTmp.append(((tmst[devItr][itr] - val) + thToaVal))
		eDevTmst.append(tmp)
		toaTmst.append(toaTmp)

	"""
	Y axis for ToA and normal plots
	"""
	y_axis = []
	y_axis_toa = []
	colorListTmst = ['k','c']
	colorListToa = ['g','m']
	markerListTmst = ['*','*']
	markerListToa = ['p','p']

	"""
	Plot the data
	"""
	for devItr in range(len(eDevTmst)):
		eDevTmst[devItr] =  np.array(eDevTmst[devItr][:20])
		toaTmst[devItr] = np.array(toaTmst[devItr][:20])
		tmp = np.array([yax[devItr] for i in range(len(eDevTmst[devItr]))])
		y_axis.append(tmp)
		tmp2 = np.array([toaYax[devItr] for i in range(len(toaTmst[devItr]))])
		y_axis_toa.append(tmp2)

		lb1 = "EndDev Tx(UpLink) Dev:..{}".format(devId[devItr][17:23]) + ' ' + SF[devItr]
		toalb1 = "Gw Rx based on th. ToA Dev:..{}".format(devId[devItr][17:23]) + ' ' + SF[devItr]
		plt.rcParams.update({'font.size': 11})
		plt.scatter(eDevTmst[devItr],y_axis[devItr], label=lb1, color=colorListTmst[devItr],marker=markerListTmst[devItr])
		plt.scatter(toaTmst[devItr],y_axis_toa[devItr], label=toalb1, color=colorListToa[devItr],marker=markerListToa[devItr])

	plt.rcParams.update({'font.size': 14})
	return tmst

def parseServTmst(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)mqtt Tx:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						#print(obj);
						res = obj.group(2);
						# print("res.split()[0]:{}".format(res.split()[0]))
						t_sec = res.split()[0]
						dev.append(t_sec)
						# itr1 += 1
				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False

def parserEndDev(file,devId):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)Tx Time:(.*)'

	devParseTmst = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(timeIdRe,line,re.M|re.I)
						#print(obj);
						res = obj.group(2)
						# print("res:{}\n".format(res))
						# print("res.split()[0]:{}".format(res.split()[0]))
						t_sec = res.split()[0]
						dev.append(t_sec)
						# itr1 += 1
				devParseTmst.append(dev)
				file.seek(0)
		return devParseTmst
	else:
		return False


def fileExists(file):
    if os.path.exists(file):
        print("file:%s found\n"%file)
        return 1
    else:
        print("File:%s not found\n"%file)
        return -1


def parserFunct(file,devId):
	devREx = r'(.*)Device ID:(.*)'
	mtimeRegEx = r'(.*)Msg Time: (.*)'

	dev2 = []
	dev1 = []
	devParse = []

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for devItr in range(len(devId)):
				dev = []
				for line in file:
					if devId[devItr] in line:
						obj = re.search(mtimeRegEx,line,re.M|re.I)
						res = obj.group(2)
						tp = dp.parse(res.split()[0])
						tmp = tp.astimezone(timezone('US/Pacific'))
						t_sec = tmp.strftime('%s')
						dev.append(t_sec)
				devParse.append(dev)
				file.seek(0)
		return devParse
	else:
		return False

'''
# this is for the toa item in the marconi log file
'''
def parserFunctToa(file,devList):

	regex_dev_toa = r'(.*)TOA:(.*)'
	dev1_val = []
	dev2_val = []
	#time_val = []
	itr = 0;
	if fileExists(file) != -1:
		with open(file,'r') as file:
			for line in file:
				if devList[0] in line and "TOA" in line:
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev1_val.append(int(res.split()[0]))
				elif devList[1] in line and "TOA" in line:
					#elif devList[1] in line and "TOA" in line:
					#obj = re.search(regex_dev_toa,line,re.M|re.I)
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev2_val.append(int(res.split()[0]))
		return (dev1_val,dev2_val)
	else:
		return False

'''
To plot the mqtt_sub timestamps
'''
def creatTimeList(file,devId,eDevTmst):
	regex_dev_sub_time = r'(.*)Msg Time:(.*)'
	parseTmst = []

	with open(file,'r') as fp:
		for devItr in range(len(devId)):
			dev = []
			val = eDevTmst[devItr][0]
			for line in fp:
				if devId[devItr] in line:
					obj = re.search(regex_dev_sub_time,line,re.M|re.I)
					res = obj.group(2)
					dev.append((int(res.split()[0])) - val)
			parseTmst.append(dev)
			fp.seek(0)
	return parseTmst

def plot(filePath,devId,eDevTmst,yax,SF):
	retFlag = 0
	#retFlag += fileExists(filePath[0])
	retFlag += fileExists(filePath)

	if retFlag < 1:
		return -1

	#timeList1 = creatTimeList(filePath[0])
	tmst = creatTimeList(filePath,devId,eDevTmst)

	# print("Delta Time between End Dev1[fe37] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev1[:20]))
	# print("Delta Time between End Dev2[fe36] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev2[:20]))
	y_axis = []
	colorList = ['c','r']
	plotCList = ['r','m']
	tmstnp = []
	for devItr in range(len(devId)):
		tmptmst = np.array(tmst[devItr])
		tmp = np.array([yax[devItr] for i in range(len(tmst[devItr]))])
		y_axis.append(tmp)
		tmstnp.append(tmptmst)

		lb1 = 'mqtt sub Dev:..{}'.format(devId[devItr][17:23]) + ' ' + SF[devItr]

		plt.plot(tmstnp[devItr][:20], y_axis[devItr][:20], color=plotCList[devItr], linestyle='None', markersize = 10.0)
		plt.scatter(tmstnp[devItr][:20],y_axis[devItr][:20],label = lb1 ,color=colorList[devItr])

	return tmstnp


if __name__ == '__main__':

	# filePath1 = ['/home/iot/Desktop/testJuypter/test0422/gw_tmst_0422_dr0dr1.txt','/home/iot/Desktop/testJuypter/test0422/log_mqtt_marconi_sub_0422_dr0dr1.txt']
	# filePath2 = ['/home/iot/Desktop/testJuypter/test0422/gw_tmst_0422_dr2dr3.txt','/home/iot/Desktop/testJuypter/test0422/log_mqtt_marconi_sub_0422_dr2dr3.txt']
	# filePath3 = ['/home/iot/Desktop/testJuypter/test0422/gw_tmst_0422_dr4.txt','/home/iot/Desktop/testJuypter/test0422/log_mqtt_marconi_sub_0422_dr4.txt']

	filePath1 = ['/home/iot/Desktop/testJuypter/test0706/gwTmstLog0706.txt','/home/iot/Desktop/testJuypter/test0706/logMqttSub0706.txt','/home/iot/Desktop/testJuypter/test0706/endDevTmstLog0706.txt']

	devId = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36']
	axis_sub = [1,3]
	axis_gw = [2,4]
	SF = ['SF8','SF9']

	thToA = {'SF10':206.847992,'SF9':308.223999,'SF8':358.912018,'SF7':374.016022,'SF8BW500':163.968002}

	fig = plt.figure(1)
	ax = plt.subplot(111)


	endDevTmst = plotEndDevTmst(filePath1[2],[4,8],[3,7],SF,thToA,devId)
	mqttSubTmst = plot(filePath1[1],devId,endDevTmst,[1,5],SF) # mqtt sub/pub plot
	gwTmst = plot_predicted(devId,filePath1[0],endDevTmst,[2,6],SF) # gw Rx

	plt.rcParams.update({'font.size': 12})
	plt.title('End Dev Tx vs Gateway Rx Time vs mqtt sub(marconi) time for 2 End Dev Transmitting max PL operating at Data Rates [DR1 & DR2]')
	plt.rcParams.update({'font.size': 16})
	text_str = 'Data Transfer Path: mdot Tx -> Gateway Rx -> Gateway MQTT pub -> marconi MQTT sub\nThe above graph is the representation of 1st 20 tmst. from start of Tx from End Dev\n'
	fig.text(.5,0.01,text_str,wrap=True,ha='center')

	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


	plt.gca().yaxis.set_major_locator (plt.NullLocator())
	plt.grid(True)
	plt.show()

	deltaTime,std = getMeanStd(endDevTmst,gwTmst,devId)
	timeDiff = interArrivalTimeDiff(filePath1[0], devId)
	print("time Diff:\n{}\n".format(timeDiff))
