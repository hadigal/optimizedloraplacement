import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import re
import time, datetime, pytz
from pytz import timezone
import dateutil.parser as dp
from statistics import mean,stdev

"""
File check
"""
# def checkFile(path):
#     if os.path.exists(path):
#         print("Found file:%s"%path)
#         return True
#     else:
#         print("File not found\n")
#         return False

def getMeanStd(endDevTmst,serverTmst,devName=['dev1','dev2']):

	dev = []
	for devItr in range(len(endDevTmst)):
		deltaT = []
		dev.append(deltaT)
		for itr in range(len(endDevTmst[devItr])):
			delta = serverTmst[devItr][itr] - endDevTmst[devItr][itr]
			dev[devItr].append(delta)

	print("\nThe mean of gw tmst with respect to end Dev Tx tmst:\n")
	colorList = ['c','b']
	devMean = []
	std = []
	for itr in range(len(devName)):
		devMean.append(mean(dev[itr]))
		std.append(stdev(dev[itr]))
		print("Dev:{} -> Mean: {}\t Std. Dev.:{}".format(devName[itr],devMean[itr],std[itr]))

		# yax = np.array([(itr+1) for i in range(len(dev[itr]))])
		# xax = np.array(dev[itr])

		# plt.plot(xax,yax,color=colorList[itr])
		# plt.scatter(xax,yax,color=colorList[itr])

	return devMean,std

"""
Plot the gw rx timestamps and toa timestamps
"""
def plot_predicted(path,tmstDev1,tmstDev2,yax,sf37,sf36):
	devEUIs = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36']

	# Gateway RX
	dev1,dev2 = parserFunct(path[0])

	d_time1 = []
	d_time2 = []

	for dup_time1 in dev1:
		d_time1.append(int(dup_time1))

	for dup_time2 in dev2:
		d_time2.append(int(dup_time2))


	# print("Time_dup1:{}\n".format(d_time1))
	# print("Time_dup2:{}\n".format(d_time2))

	# save mqtt gateway rx time in epoch format
	prog_time1 = []
	#itr1 = d_time1[0]
	prog_time2 = []
	#itr2 = d_time2[0]

	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	for itr1 in range(len(d_time1)):
		# tmp = d_time1[itr1] - tmstDev1[itr1]
		# tmp2 = d_time1[itr1] - d_time1[0]
		# tmp2 += tmp
		# prog_time1.append(tmp2)
		prog_time1.append(d_time1[itr1])
		if itr1 == 0:
			val1 = tmstDev1[0]
		prog_time1[itr1] -= val1


	print("Delta time between End Dev 1[fe37] Tx and GW Rx tmst:\n{}\n".format(prog_time1[:20]))

	for itr2 in range(len(d_time2)):
		# tmp = d_time2[itr2] - tmstDev2[itr2]
		# print("tmp:{}".format(tmp))
		# tmp2 = d_time2[itr2] - d_time2[0]
		# tmp2 += tmp
		# prog_time2.append(tmp2)
		prog_time2.append(d_time2[itr2])
		if itr2 == 0:
			val = tmstDev2[0]
		prog_time2[itr2] -= val

	print("Delta time between End Dev 1[fe36] Tx and GW Rx tmst:\n{}\n".format(prog_time2[:20]))

	ret1 = d_time1
	ret2 = d_time2

	prog_time1 = np.array(prog_time1[:20])
	prog_time2 = np.array(prog_time2[:20])

	y_axis1 = np.array([yax[0] for i in range(len(prog_time1))])
	y_axis2 = np.array([yax[1] for i in range(len(prog_time2))])

	gw = [ret1,ret2]
	eDev = [tmstDev1,tmstDev2]

	devMean,std = getMeanStd(eDev,gw,devEUIs)

	lb1 = 'GW Rx(UpLink) Dev:..fe37' + ' ' + sf37+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[0])+'\n'+'Std. Dev:'+"{:.3f}".format(std[0])
	lb2 = 'GW Rx(UpLink) Dev:..36' + ' ' + sf36+'\n'+'Mean Delta t in sec:'+"{:.3f}".format(devMean[1])+'\n'+'Std. Dev:'+"{:.3f}".format(std[1])


	plt.rcParams.update({'font.size': 11})
	plt.scatter(prog_time1[:20],y_axis1, label=lb1, color='g',marker='+')

	#plt.scatter(prog_time2[:20],y_axis2, label='Gateway Rx(UpLink) Dev:0080000004009802', color='b')
	if 'NULL' not in sf36:
		plt.scatter(prog_time2[:20],y_axis2, label=lb2, color='b',marker='+')



	plt.rcParams.update({'font.size': 14})
	plt.xlabel('Time in secs')
	plt.ylabel('DR0 vs DR3')

	return ret1,ret2

def plotEndDevTmst(file,yax,sfDev1,sfDev2):
	# end dev tx
	endDev1, endDev2 = parserEndDev(file[2])

	t1 = []
	t2 = []

	for dup_time1 in endDev1:
		t1.append(int(dup_time1))

	for dup_time2 in endDev2:
		t2.append(int(dup_time2))


	# print("End Dev: Time_dup1:{}\n".format(t1))
	# print("End Dev: Time_dup2:{}\n".format(t2))

	# save mqtt gateway rx time in epoch format
	pt1 = []
	#itr1 = d_time1[0]
	pt2 = []
	#itr2 = d_time2[0]

	# this to convert the duplicate flag: NO packet timestamps to plot grap in progressive manner
	for itr1 in range(len(t1)):
		pt1.append(t1[itr1])
		if itr1 == 0:
			val1 = t1[0]
		pt1[itr1] -= val1


	print("End Dev 1[fe37] Tx Tmst:\n{}\n".format(pt1[:20]))

	for itr2 in range(len(t2)):
		pt2.append(t2[itr2])
		if itr2 == 0:
			val = t2[0]
		pt2[itr2] -= val

	print("End Dev 1[fe36] Tx Tmst:\n{}\n".format(pt2[:20]))

	time_list1 = []

	ret1 = t1
	ret2 = t2
	pt1 = np.array(pt1[:20])
	pt2 = np.array(pt2[:20])

	y_axis1 = np.array([yax[0] for i in range(len(pt1))])
	y_axis2 = np.array([yax[1] for i in range(len(pt2))])

	lb1 = 'EndDev Tx(UpLink) Dev:..fe37' + ' ' + sfDev1
	lb2 = 'EndDev Tx(UpLink) Dev:..fe36' + ' ' + sfDev2

	plt.rcParams.update({'font.size': 11})
	plt.scatter(pt1[:20],y_axis1, label=lb1, color='k',marker='*')
	#plt.scatter(prog_time2[:20],y_axis2, label='Gateway Rx(UpLink) Dev:0080000004009802', color='b')
	if 'NULL' not in sfDev2:
		plt.scatter(pt2[:20],y_axis2, label=lb2, color='c', marker='*')

	plt.rcParams.update({'font.size': 14})
	# plt.xlabel('Time in secs')
	# plt.ylabel('DR0 v/s DR3')


	return ret1,ret2



def parserEndDev(file):
	devNameRe = r'(.*)Device ID:(.*)'
	timeIdRe = r'(.*)Tx Time:(.*)'

	dev2Tmst = []
	dev1Tmst = []

	itr1 = 0
	itr2 = 0

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for line in file:
				if "00-80-00-00-00-00-fe-37" in line:
					obj = re.search(timeIdRe,line,re.M|re.I)
					#print(obj);
					res = obj.group(2);
					# print("res.split()[0]:{}".format(res.split()[0]))
					t_sec = res.split()[0]
					dev1Tmst.append(t_sec)
					itr1 += 1
				elif "00-80-00-00-00-00-fe-36" in line:
					obj = re.search(timeIdRe,line,re.M|re.I)
					#print(obj);
					res = obj.group(2);
					# print("res.split()[0]:{}".format(res))
					t_sec = res.split()[0]
					dev2Tmst.append(t_sec)
					itr2 += 1
		return dev1Tmst,dev2Tmst
	else:
		return False,False


def fileExists(file):
    if os.path.exists(file):
        print("file:%s found\n"%file)
        return 1
    else:
        print("File:%s not found\n"%file)
        return -1


def parserFunct(file):
	regex2 = r'(.*)Device ID:(.*)'
	regex3 = r'(.*)Msg Time: (.*)'
	dev2 = []
	dev1 = []

	val1 = []
	val2 = []
	itr1 = 0
	itr2 = 0

	if fileExists(file) != -1:
		with open(file,'r') as file:
			for line in file:
				if "00-80-00-00-00-00-fe-37" in line:
					obj = re.search(regex3,line,re.M|re.I)
					#print(obj);
					res = obj.group(2);
					# print("res.split()[0]:{}".format(res.split()[0]))
					tp = dp.parse(res.split()[0])
					# t_sec = tp.strftime('%s')
					tmp = tp.astimezone(timezone('US/Pacific'))
					t_sec = tmp.strftime('%s')

					dev1.append(t_sec)
					itr1 += 1
				elif "00-80-00-00-00-00-fe-36" in line:
					obj = re.search(regex3,line,re.M|re.I)
					#print(obj);
					res = obj.group(2);
					# print("res.split()[0]:{}".format(res))
					tp = dp.parse(res.split()[0])

					tmp = tp.astimezone(timezone('US/Pacific'))
					t_sec = tmp.strftime('%s')
					dev2.append(t_sec)
					itr2 += 1
		return dev1,dev2
	else:
		return False

'''
# this is for the toa item in the marconi log file
'''
def parserFunctToa(file,devList):

	regex_dev_toa = r'(.*)TOA:(.*)'
	#regex_dev_msg_rx = r'(.*)Msg Time:(.*)'
	#regex2 = r'(.*) Duplicate:(.*)'
	#regex3 = r'(.*)|INFO|(.*)'
	dev1_val = []
	dev2_val = []
	#time_val = []
	itr = 0;
	if fileExists(file) != -1:
		with open(file,'r') as file:
			for line in file:
				if devList[0] in line and "TOA" in line:
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev1_val.append(int(res.split()[0]))
				elif devList[1] in line and "TOA" in line:
					#elif devList[1] in line and "TOA" in line:
					#obj = re.search(regex_dev_toa,line,re.M|re.I)
					obj = re.search(regex_dev_msg_rx,line,re.M|re.I)
					res = obj.group(2)
					dev2_val.append(int(res.split()[0]))
		return (dev1_val,dev2_val)
	else:
		return False

'''
To plot the mqtt_sub timestamps
'''
def creatTimeList(file,devEUIs,dev1,dev2):
	regex_dev_sub_time = r'(.*)Msg Time:(.*)'
	timeList_dev1 = []
	timeList_dev2 = []
	itr = 0
	itr2 = 0
	val1 = 0
	val2 = 0

	with open(file,'r') as fp:
		for line in fp:
			if devEUIs[0] in line:
				obj = re.search(regex_dev_sub_time,line,re.M|re.I)
				res = obj.group(2)
				timeList_dev1.append(int(res.split()[0]))
				if itr == 0:
					val1 = dev1[0]
				timeList_dev1[itr] -= val1
				itr += 1
			elif devEUIs[1] in line:
				obj = re.search(regex_dev_sub_time,line,re.M|re.I)
				res = obj.group(2)
				timeList_dev2.append(int(res.split()[0]))
				if itr2 == 0:
					val2 = dev2[0]
				timeList_dev2[itr2] -= val2
				itr2 += 1
	return timeList_dev1,timeList_dev2

def plot(filePath,devEUIs,dev1Tmst,dev2Tmst,yax,sf37,sf36):
	retFlag = 0
	#retFlag += fileExists(filePath[0])
	retFlag += fileExists(filePath[1])

	if retFlag < 1:
		return -1

	#timeList1 = creatTimeList(filePath[0])
	timeList_dev1,timeList_dev2 = creatTimeList(filePath[1],devEUIs,dev1Tmst,dev2Tmst)

	print("Delta Time between End Dev1[fe37] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev1[:20]))
	print("Delta Time between End Dev2[fe36] Tx and mqtt sub tmst\n:{}\n".format(timeList_dev2[:20]))


	#timeList1 = np.array(timeList1)
	timeList_dev1 = np.array(timeList_dev1)
	timeList_dev2 = np.array(timeList_dev2)

	y_axis1 = np.array([yax[0] for i in range(len(timeList_dev1))])
	y_axis2 = np.array([yax[1] for i in range(len(timeList_dev2))])

	lb1 = 'mqtt sub Dev:..fe37' + ' ' + sf37
	lb2 = 'mqtt sub Dev:..fe36' + ' ' + sf36
	#fig = plt.figure(figsize=(18, 16), dpi= 100, facecolor='w', edgecolor='k')
	# fig = plt.figure()

	#fig.canvas.draw()
	plt.plot(timeList_dev1[:20], y_axis1[:20], color='r', linestyle='None', markersize = 10.0)
	plt.scatter(timeList_dev1[:20],y_axis1[:20],label = lb1 ,color='c')
	plt.plot(timeList_dev2[:20], y_axis2[:20], color='m', linestyle='None', markersize = 10.0)
	#plt.scatter(timeList_dev2[:20],y_axis2[:20], label = 'mqtt sub(marconi) Dev:0080000004009802',color='r')
	if 'NULL' not in sf36:
		plt.scatter(timeList_dev2[:20],y_axis2[:20], label = lb2,color='r')
	#text_str = 'Data Transfer Path: marconi MQTT pub -> Gateway MQTT sub -> Gateway Tx -> mdot Rx\nData Transfer Path: mdot Tx -> Gateway Rx -> Gateway MQTT pub -> marconi MQTT sub\n'
	#plt.title('Gateway Rx Time vs mqtt sub(marconi) time for dev:008000000000fe37 max PL: 11B @ SF10BW125 vs mqtt sub(marconi) dev:008000000000fe36 max PL: 242B @ SF8BW500')

	return timeList_dev1,timeList_dev2


# def getMean(endDevTmst,serverTmst,devName):
#
# 	dev = []
# 	for devItr in range(len(endDevTmst)):
# 		deltaT = []
# 		dev.append(deltaT)
# 		for itr in range(len(endDevTmst[devItr])):
# 			delta = serverTmst[devItr][itr] - endDevTmst[devItr][itr]
# 			dev[devItr].append(delta)
#
# 	print("The mean of gw tmst with respect to end Dev Tx tmst:\n")
# 	colorList = ['c','b']
# 	for itr in range(len(devName)):
# 		print("Dev:{} -> Mean: {}".format(devName[itr],mean(dev[itr])))
# 		yax = np.array([(itr+1) for i in range(len(dev[itr]))])
# 		xax = np.array(dev[itr])
# 		plt.plot(xax,yax,color=colorList[itr])
# 		plt.scatter(xax,yax,color=colorList[itr])
#
# 	return dev


if __name__ == '__main__':

	# filePath1 = ['/home/iot/Desktop/testJuypter/test0422/gw_tmst_0422_dr0dr1.txt','/home/iot/Desktop/testJuypter/test0422/log_mqtt_marconi_sub_0422_dr0dr1.txt']
	# filePath2 = ['/home/iot/Desktop/testJuypter/test0422/gw_tmst_0422_dr2dr3.txt','/home/iot/Desktop/testJuypter/test0422/log_mqtt_marconi_sub_0422_dr2dr3.txt']
	# filePath3 = ['/home/iot/Desktop/testJuypter/test0422/gw_tmst_0422_dr4.txt','/home/iot/Desktop/testJuypter/test0422/log_mqtt_marconi_sub_0422_dr4.txt']

	filePath1 = ['/home/iot/Desktop/testJuypter/test0526/gwTmstLog0526.txt','/home/iot/Desktop/testJuypter/test0526/logMqttSub0526.txt','/home/iot/Desktop/testJuypter/test0526/endDevTmstLog0526.txt']

	devEUIs = ['00-80-00-00-00-00-fe-37','00-80-00-00-00-00-fe-36']
	axis_sub = [1,3]
	axis_gw = [2,4]

	fig = plt.figure(1)
	ax = plt.subplot(111)


	dev1Tmst,dev2Tmst = plotEndDevTmst(filePath1,[3,6],'SF7','SF10')

	ret1,ret2 = plot(filePath1,devEUIs,dev1Tmst,dev2Tmst,[1,4],'SF7','SF10') # mqtt sub/pub plot
	gw1,gw2 = plot_predicted(filePath1,dev1Tmst,dev2Tmst,[2,5],'SF7','SF10')


	# ret1,ret2 = plot(filePath2,devEUIs,[7,5],'SF7','SF8') # mqtt sub/pub plot
	# plot_predicted(filePath2,ret1,ret2,[8,6],'SF7','SF8')
	# ret1,ret2 = plot(filePath3,devEUIs,[9,0],'SF8BW500','NULL') # mqtt sub/pub plot
	# plot_predicted(filePath3,ret1,ret2,[10,0],'SF8BW500','NULL')

	plt.rcParams.update({'font.size': 12})
	plt.title('End Dev Tx vs Gateway Rx Time vs mqtt sub(marconi) time for 2 End Dev Transmitting max PL operating at Data Rates [DR0 & DR3]')
	plt.rcParams.update({'font.size': 16})
	text_str = 'Data Transfer Path: mdot Tx -> Gateway Rx -> Gateway MQTT pub -> marconi MQTT sub\nThe above graph is the representation of 1st 20 tmst. from start of Tx from End Dev\n'
	fig.text(.5,0.01,text_str,wrap=True,ha='center')

	# plt.gca().legend(loc='best')
	# plt.gca().legend(bbox_to_anchor=(1.04,1), loc="upper left")

	# ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),ncol=3, fancybox=True, shadow=True)
	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


	plt.gca().yaxis.set_major_locator (plt.NullLocator())
	plt.grid(True)
	plt.show()

	endDevTmst = [dev1Tmst,dev2Tmst]
	gwTmst = [gw1,gw2]
	# for itr in range(2):
	# 	endDevTmst.append(dev)

	# fig2 = plt.figure(2)
	deltaTime,std = getMeanStd(endDevTmst,gwTmst,devEUIs)
	# plt.title("mean scatter plot")
	# plt.show()
