clear all
close all
clc

%       f       SF7                     SF8                     SF9                     SF10
f_rho = [
    0.033       0.9846153846153847        0.990990990990991        0.9910714285714286        0.9911504424778761;
    0.067       0.993103448275862        0.9952830188679245         0.9954128440366973        0.9953703703703703;
    0.1         0.9936305732484076        0.9965986394557823        0.9966329966329966        0.9966442953020134;
    0.2         0.9958333333333333        0.9979959919839679        0.9980392156862745        0.9980657640232108;
    1           0.9973333333333333        1.0034682080924855        1.0056864337936637        1.00724112961622;
    ];

%        Ws
SF8 = [
    0.9999999999999979;
    0.9999999999999885;
    0.9999999999999989;
    0.999999999999999;
    -0.3333333333333388;
    ];

SF7	= [
    1.0000000000000036;
    0.999999999999998;
    1.0000000000000002;
    1.0000000000000036;
    0.9999999999999852;
    ];

SF9	= [
    1.0000000000000036;
    1.000000000000006;
    1.0000000000000047;
    1.0000000000000036;
    -0.14285714285714166;
    ];

SF10 = [
    0.9999999999999984;
    0.9999999999999956;
    0.9999999999999849;
    0.9999999999999778;
    -0.10000000000000134;
    ];

C = {'r','g','b','k'};
Cdot = {'ro','go','bo','ko'};
Cdash = {'r:','g:','b:','k:'};
figure
hold on

for sfi = 2:5,
    plot(f_rho(:,1),f_rho(:,sfi),C{sfi-1},'LineWidth',2);
end

for sfi = 2:5,
    plot(f_rho(:,1),f_rho(:,sfi),Cdot{sfi-1},'LineWidth',2);
end
grid on
set(gcf,'color','w');
legend('SF7','SF8','SF9','SF10','Location','SouthEast');
xlabel('Metering Uplink Frequency, Hz [solid line]');
ylabel('Traffic Intensity \rho = \lambda/\mu');
ax1 = gca; % current axes
ax1.XColor = 'r';
ax1.YColor = 'r';

ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,...
    'YAxisLocation','right',...
    'Color','none');
x2 = 1:0.2:20;
y2 = x2.^2./x2.^3;
% line(SF7(:,1),SF7(:,2),'Parent',ax2,'Color',C{1},'LineStyle',':','LineWidth',2);
% line(SF8(:,1),SF8(:,2),'Parent',ax2,'Color',C{2},'LineStyle',':','LineWidth',2);
% line(SF9(:,1),SF9(:,2),'Parent',ax2,'Color',C{3},'LineStyle',':','LineWidth',2);
% line(SF10(:,1),SF10(:,2),'Parent',ax2,'Color',C{4},'LineStyle',':','LineWidth',2);
% xlabel('L_{s}, Average Number of LoRaWAN Packets in the Gateway Queue [dotted line]','Parent',ax2);

line(f_rho(:,1),SF7(:,1),'Parent',ax2,'Color',C{1},'LineStyle',':','LineWidth',2);
line(f_rho(:,1),SF8(:,1),'Parent',ax2,'Color',C{2},'LineStyle',':','LineWidth',2);
line(f_rho(:,1),SF9(:,1),'Parent',ax2,'Color',C{3},'LineStyle',':','LineWidth',2);
line(f_rho(:,1),SF10(:,1),'Parent',ax2,'Color',C{4},'LineStyle',':','LineWidth',2);

ylabel('W_{s}, Average Packet Wait Time (Service Delay), seconds','Parent',ax2);




