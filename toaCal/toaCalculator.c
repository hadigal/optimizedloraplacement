#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

static inline float getSysmbolDuration(uint8_t sf, uint64_t bw)
{
  uint32_t numr = 1 << sf;
  float ts = ((float)numr)/((float)bw);

  return ts;
}

static inline float getTimePayload(uint16_t pl, uint8_t sf, uint8_t crc, uint8_t cr, uint8_t h, uint64_t bw, uint8_t de)
{
  float tpl;
  float ts = getSysmbolDuration(sf,bw);

  uint32_t denom = 4*(sf - (2*de));
  uint32_t numr = 8*pl - (4*sf) + 28 - (16*crc) - (20*h);

  float val = 8 + fmax(((ceil(numr/denom))*(cr+4)),0);

  tpl = ts*val;

  return tpl;
}

int main(void)
{
  uint8_t header;
  uint8_t crc;
  uint8_t sf;
  uint8_t cr;
  uint8_t de;
  uint16_t plSize;
  uint16_t symb;
  uint64_t bw;

  float ts;
  float tpl;
  float tpre;
  float toa;

  printf("\n******************* ToA Calculator: *******************\n");
  printf("\npayload size in Bytes:");
  scanf("%hu",&plSize);
  printf("\nSF:");
  scanf("%hhu",&sf);
  printf("\nCR:");
  scanf("%hhu",&cr);
  printf("\nCRC:");
  scanf("%hhu",&crc);
  printf("\nHeader:");
  scanf("%hhu",&header);
  printf("\nBW:");
  scanf("%lu",&bw);
  printf("\nNum. of symbols:");
  scanf("%hu",&symb);
  printf("\nDE:");
  scanf("%hhu",&de);

  ts = getSysmbolDuration(sf,bw);

  tpre = (symb + 4.25)*ts;
  //printf("------tpre:%f",tpre);
  tpl = getTimePayload(plSize,sf,crc,cr,header,bw,de);
  //printf("------tpl:%f",tpl);
  toa = tpre+tpl;

  printf("\n\nThe ToA based on above data:%f sec or %f msec\n",toa,(toa*1000));
  printf("\n******************************************************\n");
  return EXIT_SUCCESS;
}
